<?php 
/**
 * Medidas de crecimiento standard OMS
 * array content: days[0],sdn4[1],sdn3[2],sdn2[3],sdn1[4],sd0[5],sd1[6],sd2[7],sd3[8],sd4[9]
 */
 class MedidasOMS
 {
 	public static function get($resource){
 		$path = Yii::getPathOfAlias('application.data');
 		$filename = $path.DIRECTORY_SEPARATOR.$resource.'.dat';
 		$content = @file_get_contents($filename);
 		$content = explode("\n", $content);
 		$data = array();
 		foreach ($content as $key => $value) {
 			if(!$key) continue;
 			$data[] = explode("\t", $value);
 		}
 		return $data;
 	}

 } ?>