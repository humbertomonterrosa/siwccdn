<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user = Usuario::model()->find('username=? AND estado=1',array($this->username));

		if(!$user)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		elseif($user->password!==sha1($this->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else{
			Yii::app()->user->setState('user',$user);
			$this->errorCode=self::ERROR_NONE;
		}
		return !$this->errorCode;
	}

}