<?php
 
 /**
  * Citas Medicas
  */
 class CitasController extends Controller
 {

    public $tab = 'citas';
    public $title = 'Citas';
    public function actionIndex()
    {
        $usuario_id = Yii::app()->user->getState('user')->id;
        $citas = CitaMedica::model()->findAll('usuario_id=? AND estado!=2',array($usuario_id));
        $events = array();
        foreach ($citas as $key => $value) {
            $events[] = array("title"=>$value->paciente->getNombreCompleto().' - '.$value->motivo,"start"=>$value->fechahora,"paciente"=>array("nombre"=>$value->paciente->getNombreCompleto()));
            
        }

        $this->render('index',array('events'=>json_encode($events)));   
    }

    public function actionAgendar($id){
    	$paciente = $this->loadPaciente($id);
        $cita = CitaMedica::model()->find('estado=0 AND paciente_id=? AND fechahora > now()',array($id));
        if(!$cita){
        	$medico = Usuario::model()->findAll('estado=1 and rol !=0');
        	$model = new CitaMedica;
        	if(isset($_POST['CitaMedica'])){
        		$model->attributes = $_POST['CitaMedica'];
                $model->fechahora = $model->fecha." ".$model->hora;
                $model->paciente_id = $id;
                $model->usuario_id = Yii::app()->user->getState('user')->id;
                $model->estado = 0;
        		if($model->save()){
        			Yii::app()->user->setFlash('success','La cita se agendó exitosamente. <a href="'.$this->createUrl('/citas/imprimir',array('id'=>$id)).'" target="_blank" class="btn btn-default"><i class="glyphicon glyphicon-print"></i> Impimir comprobante</a>');
        			$this->redirect(array('/paciente/'.$id));
        		}
        		else{
        			Yii::app()->user->setFlash('error','Error al agendar la cita, intente mas tarde. '.print_r($model->getErrors(),true));
        		}
        	}
            else{
                $model->motivo = 'Próxima cita de control '.$paciente->getProximoControl()->titulo;
            }
           $this->render('agendar',array('paciente'=>$paciente,'medico'=>$medico,'model'=>$model));

        }
        else{
    	   $this->render('agendar',array('paciente'=>$paciente,'cita'=>$cita));
        }

    }

    public function actionCancelar($id){
        $paciente = $this->loadPaciente($id);

        $cita = CitaMedica::model()->find('estado=0 AND paciente_id=? AND fechahora > now()',array($id));
        if($cita){
            if(isset($_POST['confirm'])){
                $cita->estado = 2;
                $cita->fecha = date('Y-m-d');
                $cita->hora = date('H:i:s');
                if($cita->save()){
                    $this->redirect(array('/citas/agendar/'.$id));
                }
                else{
                    Yii::app()->user->setFlash('error','La cita no pudo ser cancelada.');
                }
            }
        }

        $this->render('cancelar',array('cita'=>$cita));

    }

    public function actionImprimir($id){
        $paciente = $this->loadPaciente($id);

        $cita = CitaMedica::model()->find('estado=0 AND paciente_id=? AND fechahora > now()',array($id));
        if(!$cita){
            echo "El paciente no tiene cita disponible para imprimir.";
        }
        else{
            echo $this->renderPartial('imprimir',array('paciente'=>$paciente,'cita'=>$cita),true);
        }
    }

    public function loadPaciente($id)
    {
        $model=Paciente::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
 
 	
 	public function filters()
 	{
 		return array(
 			'accessControl',
 		);
 	}
 
 	public function accessRules()
 	{
 		// return external action classes, e.g.:
        return array(
            array('allow','users'=>array('@')),
            array('deny','users'=>array('*'))
            );
 	}
 	
 } ?>