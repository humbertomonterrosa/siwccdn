<?php

/**
 * Adminsitra todos los pacientes del programa.
 */
class PacienteController extends Controller
{
	public $tab = 'paciente';
	public $title = 'Pacientes';

    public function actionIndex()
    {
    	$model  = new BuscaPaciente;
    	$data = null;
    	if(isset($_POST['BuscaPaciente'])){
    		$model->attributes = $_POST['BuscaPaciente'];

    		if(!empty($model->tpdocumento)){
    			$model->setScenario('acudiente');
    		}

    		if($model->validate()){
                if(count(array_filter($model->getAttributes()))>0){
                    $data = new Paciente('search');
                    if(!empty($model->tpdocumento)){
                        $data->a_tpdocumento = $model->tpdocumento;
                        $data->a_documento = $model->documento;
                    }
                    else{
                        $data->attributes = $model->attributes;
                    }
                }
    			
    		}
    	}
     	$this->render('index',array('model'=>$model,'data'=>$data));
    }

    public function actionCreate(){
    	$model = new Paciente;
        $acudientes = array(new Acudiente);
        $evolucion = new Evolucion;
        $modelsaved = false;
    	if(isset($_POST['Paciente'])&&isset($_POST['Evolucion'])){
            $trans = Yii::app()->db->beginTransaction();
    		$model->attributes = $_POST['Paciente'];
            $evolucion->attributes = $_POST['Evolucion'];
            if($model->save()){
                $evolucion->paciente_id = $model->id;
                $evolucion->diagnosticado = Yii::app()->user->getState('user')->id;

                $evaluacion = Evaluacion::model()->find();

                $evolucion->evaluacion_control_id = $evaluacion->evaluacionControls[0]->control_id;
                $evolucion->edad = 0;
                $evolucion->fecha = date('Y-m-d H:i:s');
                if($evolucion->save()){
                    $modelsaved=true;
                }
            }

            
            if(isset($_POST['Acudientes'])&&count($_POST['Acudientes'])>0){
                $error = false;
                foreach ($_POST['Acudientes'] as $key => $value) {
                    $acudiente = new Acudiente;
                    $acudiente->attributes = $value;
                    $acudientes[$key]  = $acudiente;
                    if($acudiente->validate()){
                        if($modelsaved){
                            $search = $acudiente->searchByDI();
                            if(!$search)    $acudiente->save();
                            else $acudiente = $search;

                            $pa = new PacienteAcudiente;
                            $pa->paciente_id = $model->id;
                            $pa->acudiente_id = $acudiente->id;
                            if($pa->save()){
                                
                            }
                            else{
                                $error = true;
                            }
                        }
                    }
                    else{
                        $error = true;
                    }
                }

                if($modelsaved&&!$error){
                    $trans->commit();
                    $this->redirect(array('view','id'=>$model->id));
                }
                else{
                    $trans->rollback();
                }
            }

        }

        $this->render('create',array('model'=>$model,'acudientes'=>$acudientes,'evolucion'=>$evolucion));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);

        $evolucion = new Evolucion;
        if(isset($_POST['Evolucion'])){
            $evolucion->attributes = $_POST['Evolucion'];
            $existe = Evolucion::model()->find('paciente_id=? AND edad=?',array($id,$evolucion->edad));
            if(!$existe){
                $evolucion->fecha = date('c');
                $evolucion->evaluacion_control_id = $model->getProximoControl()->evaluacionControls[0]->id;
                $evolucion->diagnosticado = Yii::app()->user->getState('user')->id;
                $evolucion->paciente_id = $id;
                if($evolucion->save()){
                    Yii::app()->user->setFlash('success_evolucion','La evolución se ha guardado exitosamente.');
                    $this->redirect('?_r='.sha1('METADATA'.microtime()));
                }
                else{
                    Yii::app()->user->setFlash('error_evolucion','No se ha podido guardar la evolución del paciente, vuelva a intentar mas tarde.');
                }
            }
            else{
                Yii::app()->user->setFlash('error_evolucion','Ya se encuentra registrada una evolucion para el mes número: '.$evolucion->edad);
                
            }

        }
        
        $this->render('view',array(
            'model'=>$model,
            'evolucion'=>$evolucion
        ));
    }

	// -----------------------------------------------------------
	// Uncomment the following methods and override them if needed
	
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'accessControl',
		);
	}

	public function accessRules(){
		return array(
			array('allow','users'=>array('@')),
			array('deny','users'=>array('*'))
			);
	}

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Usuario the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Paciente::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }	

    public function actionVacunas($id){
        $model = $this->loadModel($id);

        $vacunas = VacunaEdad::model()->findAll();

        $this->render('vacunas',array('vacunas'=>$vacunas,'model'=>$model));
    }

    public function actionAplicarVacuna(){
        $response = array("success"=>false,"message"=>"Petición invalida");
        $vacunapaciente = new VacunaPaciente();
        if(isset($_POST['VacunaPaciente'])){
            $vacunapaciente->attributes = $_POST['VacunaPaciente'];
            if($vacunapaciente->save()){
                $response = array("success"=>true,"message"=>"Vacuna aplicada exitosamente");
            }
            else{
                $response["message"] = "Error al intentar aplicar la vacuna. ".(count($vacunapaciente->getErrors())>0?'Verifique los campos con (*) esten diligenciados':'');
            }
        }

        echo json_encode($response);
    }

    public function actionEscalaDesarrollo($id){
        $model = $this->loadModel($id);
        $grupoedad = EscalaDesarrolloEdades::getGrupoEdad($model->getEdadEnMeses()); //Grupo de edades actual del paciente
        $ref = $grupoedad->escalaDesarrolloReferenciases; // Referencias para el grupo de edades actual del paciente
        
        $escalas = EscalaDesarrollo::model()->findAll(array('order'=>'orden ASC'));
        
        // Recibiendo el formulario
        if(isset($_POST['ajax'])&&isset($_POST['ResultadosForm'])){
            $response = array("error"=>true,"message"=>"Petición invalida");
            if(isset($_POST['ResultadosForm']['item'])){
                $items = $_POST['ResultadosForm']['item'];

                $ne = count($escalas); //Numero de escalas
                $ni = count($items); // Numero de items recibidos

                if($ne==$ni){
                    $transaction = Yii::app()->db->beginTransaction();
                    $resultado =  new EscalaDesarrolloResultado;
                    $resultado->examinador = Yii::app()->user->getState('user')->id;
                    $resultado->paciente_id = $model->id;
                    $resultado->fecha = date('Y-m-d H:i:s');
                    $resultado->edad = $model->getEdadEnMeses();
                    $resultado->escala_desarrollo_edad_id = $grupoedad->id;
                    $resultado->observaciones = @$_POST['ResultadosForm']['observaciones'];

                    if($resultado->save()){
                        $error = false;
                        foreach ($items as $key => $value) {
                            $item = new EscalaDesarrolloResultadoItem;
                            $item->escala_desarrollo_resultado_id = $resultado->id;
                            $item->escala_desarrollo_item_id = $value;
                            if(!$item->save()){
                                $error = true;
                                break;
                            }
                        }

                        if(!$error){
                            $transaction->commit();
                            $response = array("error"=>false,"message"=>"Evaluación guardada.");
                        }
                        else{
                            $transaction->rollback();
                            $response["message"] = "Error al guardar los resultados, una de las evaluaciones está mal relacionada";
                        }
                    }
                    else{
                        $response["message"] = "Error al guardar los resultados";
                    }
                }
                else{
                    $response["message"] = "El número de evaluaciones de escala de desarrollo no coinciden con la cantidad de resultados recibidos";
                }
            }
            echo json_encode($response);
            return;
        }

        $this->render('escala',array('model'=>$model,'grupo'=>$grupoedad,'ref'=>$ref,'escalas'=>$escalas));
    }

    public function actionReporte($id){
        $model = $this->loadModel($id);
        $this->renderPartial('reporte',array('model'=>$model));
    }
}