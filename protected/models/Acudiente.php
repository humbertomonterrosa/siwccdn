<?php

/**
 * This is the model class for table "acudiente".
 *
 * The followings are the available columns in table 'acudiente':
 * @property integer $id
 * @property string $primer_nombre
 * @property string $segundo_nombre
 * @property string $primer_apellido
 * @property string $segundo_apellido
 * @property integer $tipo_documento
 * @property string $documento
 * @property string $ciudad
 * @property string $direccion
 * @property string $telefono
 * @property string $correo
 * @property string $parentesco
 *
 * The followings are the available model relations:
 * @property TipoDocumento $tipoDocumento
 * @property Paciente[] $pacientes
 */
class Acudiente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'acudiente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('primer_nombre, primer_apellido, segundo_apellido, tipo_documento, documento, ciudad, direccion, telefono, parentesco', 'required','message'=>'{attribute} no puede estar en blanco.'),
			array('tipo_documento', 'numerical', 'integerOnly'=>true),
			array('primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, parentesco', 'length', 'max'=>10),
			array('documento', 'length', 'max'=>15),
			array('direccion, telefono, ciudad', 'length', 'max'=>50),
			array('correo', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, tipo_documento, documento, ciudad, direccion, telefono, correo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tipoDocumento' => array(self::BELONGS_TO, 'TipoDocumento', 'tipo_documento'),
			'pacientes' => array(self::MANY_MANY, 'Paciente', 'paciente_acudiente(acudiente_id, paciente_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'primer_nombre' => 'Primer Nombre',
			'segundo_nombre' => 'Segundo Nombre',
			'primer_apellido' => 'Primer Apellido',
			'segundo_apellido' => 'Segundo Apellido',
			'tipo_documento' => 'Tipo Documento',
			'documento' => 'Documento de identidad',
			'ciudad' => 'Ciudad',
			'direccion' => 'Dirección',
			'telefono' => 'Teléfono',
			'correo' => 'Correo electrónico',
			'parentesco' => 'Parentesco'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('primer_nombre',$this->primer_nombre,true);
		$criteria->compare('segundo_nombre',$this->segundo_nombre,true);
		$criteria->compare('primer_apellido',$this->primer_apellido,true);
		$criteria->compare('segundo_apellido',$this->segundo_apellido,true);
		$criteria->compare('tipo_documento',$this->tipo_documento);
		$criteria->compare('documento',$this->documento,true);
		$criteria->compare('ciudad',$this->ciudad);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('correo',$this->correo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchByDI(){
		return self::model()->find('tipo_documento=? AND documento=?',array($this->tipo_documento,$this->documento));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Acudiente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getParentescos(){
		$arr = array('PADRE'=>'PADRE','MADRE'=>'MADRE','TIO(A)'=>'TIO(A)','PRIMO(A)'=>'PRIMO(A)','HERMANO(A)'=>'HERMANO(A)','OTRO'=>'OTRO');
		asort($arr);
		return $arr;
	}

	public function getNombres(){
		return CHtml::encode(trim($this->primer_nombre." ".$this->segundo_nombre));
	}

	public function getApellidos(){
		return CHtml::encode(trim($this->primer_apellido." ".$this->segundo_apellido));
	}

	public function getNombreCompleto(){
		return $this->getNombres()." ".$this->getApellidos();
	}
}
