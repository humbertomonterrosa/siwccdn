<?php

class BuscaPaciente extends CFormModel
{

	public $certnacidovivo;
	public $registrocivil;
	public $nombre;
	public $tpdocumento;
	public $documento;

	public function rules(){
		return array(
			array('tpdocumento,documento','required','on'=>'acudiente','message'=>'{attribute} no puede estar en blanco.'),
			array('certnacidovivo,registrocivil,nombre,tpdocumento,documento','safe')
			);
	}

	/**
	 * @return array customized attribute labels (name=&gt;label)
	 */
	public function attributeLabels()
	{
		return array(
			'certnacidovivo'=>'Certificado nacido vivo',
			'registrocivil'=>'Número de Registro Civil',
			'nombre'=>'Nombre del paciente',
			'tpdocumento'=>'Tipo Documento',
			'documento'=>'Documento de Identidad'
		);
	}
}