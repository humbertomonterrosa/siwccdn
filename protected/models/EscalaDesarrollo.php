<?php

/**
 * This is the model class for table "escala_desarrollo".
 *
 * The followings are the available columns in table 'escala_desarrollo':
 * @property integer $id
 * @property string $titulo
 * @property string $abreviatura
 * @property string $orden
 *
 * The followings are the available model relations:
 * @property EscalaDesarrolloItem[] $escalaDesarrolloItems
 */
class EscalaDesarrollo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'escala_desarrollo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('titulo, abreviatura, orden', 'required'),
			array('titulo', 'length', 'max'=>45),
			array('abreviatura', 'length', 'max'=>6),
			array('orden', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, titulo, abreviatura, orden', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'escalaDesarrolloItems' => array(self::HAS_MANY, 'EscalaDesarrolloItem', 'escala_desarrollo_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'titulo' => 'Titulo',
			'abreviatura' => 'Abreviatura',
			'orden' => 'Orden',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('titulo',$this->titulo,true);
		$criteria->compare('abreviatura',$this->abreviatura,true);
		$criteria->compare('orden',$this->orden,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EscalaDesarrollo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
