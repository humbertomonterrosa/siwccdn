<?php

/**
 * This is the model class for table "escala_desarrollo_item".
 *
 * The followings are the available columns in table 'escala_desarrollo_item':
 * @property integer $id
 * @property integer $escala_desarrollo_id
 * @property integer $grupo_edades_id
 * @property integer $valor
 * @property string $descripcion
 *
 * The followings are the available model relations:
 * @property EscalaDesarrolloEdades $grupoEdades
 * @property EscalaDesarrollo $escalaDesarrollo
 * @property EscalaDesarrolloResultado[] $escalaDesarrolloResultados
 */
class EscalaDesarrolloItem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'escala_desarrollo_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('escala_desarrollo_id, grupo_edades_id, valor, descripcion', 'required'),
			array('escala_desarrollo_id, grupo_edades_id, valor', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, escala_desarrollo_id, grupo_edades_id, valor, descripcion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'grupoEdades' => array(self::BELONGS_TO, 'EscalaDesarrolloEdades', 'grupo_edades_id'),
			'escalaDesarrollo' => array(self::BELONGS_TO, 'EscalaDesarrollo', 'escala_desarrollo_id'),
			'escalaDesarrolloResultados' => array(self::HAS_MANY, 'EscalaDesarrolloResultado', 'escala_desarrollo_item_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'escala_desarrollo_id' => 'Escala Desarrollo',
			'grupo_edades_id' => 'Grupo Edades',
			'valor' => 'Valor',
			'descripcion' => 'Descripcion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('escala_desarrollo_id',$this->escala_desarrollo_id);
		$criteria->compare('grupo_edades_id',$this->grupo_edades_id);
		$criteria->compare('valor',$this->valor);
		$criteria->compare('descripcion',$this->descripcion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EscalaDesarrolloItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
