<?php

/**
 * This is the model class for table "escala_desarrollo_referencias".
 *
 * The followings are the available columns in table 'escala_desarrollo_referencias':
 * @property integer $id
 * @property integer $escala_desarrollo_edades_id
 * @property integer $alerta_min
 * @property integer $alerta_max
 * @property integer $medio_min
 * @property integer $medio_max
 * @property integer $medio_alto_min
 * @property integer $medio_alto_max
 * @property integer $alto_min
 * @property integer $alto_max
 *
 * The followings are the available model relations:
 * @property EscalaDesarrolloEdades $escalaDesarrolloEdades
 */
class EscalaDesarrolloReferencias extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'escala_desarrollo_referencias';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('escala_desarrollo_edades_id, alerta_min, alerta_max, medio_min, medio_max, medio_alto_min', 'required'),
			array('escala_desarrollo_edades_id, alerta_min, alerta_max, medio_min, medio_max, medio_alto_min, medio_alto_max, alto_min, alto_max', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, escala_desarrollo_edades_id, alerta_min, alerta_max, medio_min, medio_max, medio_alto_min, medio_alto_max, alto_min, alto_max', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'escalaDesarrolloEdades' => array(self::BELONGS_TO, 'EscalaDesarrolloEdades', 'escala_desarrollo_edades_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'escala_desarrollo_edades_id' => 'Escala Desarrollo Edades',
			'alerta_min' => 'Alerta Min',
			'alerta_max' => 'Alerta Max',
			'medio_min' => 'Medio Min',
			'medio_max' => 'Medio Max',
			'medio_alto_min' => 'Medio Alto Min',
			'medio_alto_max' => 'Medio Alto Max',
			'alto_min' => 'Alto Min',
			'alto_max' => 'Alto Max',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('escala_desarrollo_edades_id',$this->escala_desarrollo_edades_id);
		$criteria->compare('alerta_min',$this->alerta_min);
		$criteria->compare('alerta_max',$this->alerta_max);
		$criteria->compare('medio_min',$this->medio_min);
		$criteria->compare('medio_max',$this->medio_max);
		$criteria->compare('medio_alto_min',$this->medio_alto_min);
		$criteria->compare('medio_alto_max',$this->medio_alto_max);
		$criteria->compare('alto_min',$this->alto_min);
		$criteria->compare('alto_max',$this->alto_max);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EscalaDesarrolloReferencias the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
