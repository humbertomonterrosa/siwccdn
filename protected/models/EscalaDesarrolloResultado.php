<?php

/**
 * This is the model class for table "escala_desarrollo_resultado".
 *
 * The followings are the available columns in table 'escala_desarrollo_resultado':
 * @property integer $id
 * @property integer $examinador
 * @property integer $paciente_id
 * @property string $fecha
 * @property integer $edad
 * @property integer $escala_desarrollo_edad_id
 * @property string $observaciones
 *
 * The followings are the available model relations:
 * @property EscalaDesarrolloEdades $escalaDesarrolloEdad
 * @property Paciente $paciente
 * @property Usuario $examinador0
 * @property EscalaDesarrolloResultadoItem[] $escalaDesarrolloResultadoItems
 */
class EscalaDesarrolloResultado extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'escala_desarrollo_resultado';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('examinador, paciente_id, fecha, edad, escala_desarrollo_edad_id', 'required'),
			array('examinador, paciente_id, edad, escala_desarrollo_edad_id', 'numerical', 'integerOnly'=>true),
			array('observaciones', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, examinador, paciente_id, fecha, edad, escala_desarrollo_edad_id, observaciones', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'escalaDesarrolloEdad' => array(self::BELONGS_TO, 'EscalaDesarrolloEdades', 'escala_desarrollo_edad_id'),
			'paciente' => array(self::BELONGS_TO, 'Paciente', 'paciente_id'),
			'examinador0' => array(self::BELONGS_TO, 'Usuario', 'examinador'),
			'escalaDesarrolloResultadoItems' => array(self::HAS_MANY, 'EscalaDesarrolloResultadoItem', 'escala_desarrollo_resultado_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'examinador' => 'Examinador',
			'paciente_id' => 'Paciente',
			'fecha' => 'Fecha',
			'edad' => 'Edad',
			'escala_desarrollo_edad_id' => 'Escala Desarrollo Edad',
			'observaciones' => 'Observaciones',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('examinador',$this->examinador);
		$criteria->compare('paciente_id',$this->paciente_id);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('edad',$this->edad);
		$criteria->compare('escala_desarrollo_edad_id',$this->escala_desarrollo_edad_id);
		$criteria->compare('observaciones',$this->observaciones,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EscalaDesarrolloResultado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
