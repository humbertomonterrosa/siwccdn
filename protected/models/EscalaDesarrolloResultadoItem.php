<?php

/**
 * This is the model class for table "escala_desarrollo_resultado_item".
 *
 * The followings are the available columns in table 'escala_desarrollo_resultado_item':
 * @property integer $id
 * @property integer $escala_desarrollo_resultado_id
 * @property integer $escala_desarrollo_item_id
 *
 * The followings are the available model relations:
 * @property EscalaDesarrolloItem $escalaDesarrolloItem
 * @property EscalaDesarrolloResultado $escalaDesarrolloResultado
 */
class EscalaDesarrolloResultadoItem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'escala_desarrollo_resultado_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('escala_desarrollo_resultado_id, escala_desarrollo_item_id', 'required'),
			array('escala_desarrollo_resultado_id, escala_desarrollo_item_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, escala_desarrollo_resultado_id, escala_desarrollo_item_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'escalaDesarrolloItem' => array(self::BELONGS_TO, 'EscalaDesarrolloItem', 'escala_desarrollo_item_id'),
			'escalaDesarrolloResultado' => array(self::BELONGS_TO, 'EscalaDesarrolloResultado', 'escala_desarrollo_resultado_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'escala_desarrollo_resultado_id' => 'Escala Desarrollo Resultado',
			'escala_desarrollo_item_id' => 'Escala Desarrollo Item',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('escala_desarrollo_resultado_id',$this->escala_desarrollo_resultado_id);
		$criteria->compare('escala_desarrollo_item_id',$this->escala_desarrollo_item_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EscalaDesarrolloResultadoItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
