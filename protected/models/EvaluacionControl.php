<?php

/**
 * This is the model class for table "evaluacion_control".
 *
 * The followings are the available columns in table 'evaluacion_control':
 * @property integer $id
 * @property integer $evaluacion_id
 * @property integer $control_id
 *
 * The followings are the available model relations:
 * @property Control $control
 * @property Evaluacion $evaluacion
 * @property Evolucion[] $evolucions
 */
class EvaluacionControl extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evaluacion_control';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('evaluacion_id, control_id', 'required'),
			array('evaluacion_id, control_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, evaluacion_id, control_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'control' => array(self::BELONGS_TO, 'Control', 'control_id'),
			'evaluacion' => array(self::BELONGS_TO, 'Evaluacion', 'evaluacion_id'),
			'evolucions' => array(self::HAS_MANY, 'Evolucion', 'evaluacion_control_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'evaluacion_id' => 'Evaluacion',
			'control_id' => 'Control',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('evaluacion_id',$this->evaluacion_id);
		$criteria->compare('control_id',$this->control_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EvaluacionControl the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
