<?php

/**
 * This is the model class for table "evolucion".
 *
 * The followings are the available columns in table 'evolucion':
 * @property integer $id
 * @property integer $paciente_id
 * @property integer $evaluacion_control_id
 * @property string $peso
 * @property string $talla
 * @property integer $perimetro_cefalico
 * @property string $observaciones
 * @property integer $diagnosticado
 *
 * The followings are the available model relations:
 * @property Usuario $diagnosticado0
 * @property EvaluacionControl $evaluacionControl
 * @property Paciente $paciente
 */
class Evolucion extends CActiveRecord
{
	public $edad;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'evolucion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('paciente_id, evaluacion_control_id, peso, talla, perimetro_cefalico, observaciones, diagnosticado, fecha,edad', 'required','message'=>'{attribute} no puede estar en blanco.'),
			array('paciente_id, evaluacion_control_id, diagnosticado', 'numerical', 'integerOnly'=>true),
			array('peso, talla', 'length', 'max'=>10),
			array('peso, talla, perimetro_cefalico','numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, paciente_id, evaluacion_control_id, peso, talla, perimetro_cefalico, observaciones, diagnosticado, fecha', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'diagnosticado0' => array(self::BELONGS_TO, 'Usuario', 'diagnosticado'),
			'evaluacionControl' => array(self::BELONGS_TO, 'EvaluacionControl', 'evaluacion_control_id'),
			'paciente' => array(self::BELONGS_TO, 'Paciente', 'paciente_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'paciente_id' => 'Paciente',
			'evaluacion_control_id' => 'Evaluacion Control',
			'peso' => 'Peso (Kg)',
			'talla' => 'Talla (cm)',
			'perimetro_cefalico (cm)' => 'Perimetro Cefalico',
			'observaciones' => 'Observación Medica',
			'diagnosticado' => 'Diagnosticado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('paciente_id',$this->paciente_id);
		$criteria->compare('evaluacion_control_id',$this->evaluacion_control_id);
		$criteria->compare('peso',$this->peso,true);
		$criteria->compare('talla',$this->talla,true);
		$criteria->compare('perimetro_cefalico',$this->perimetro_cefalico);
		$criteria->compare('observaciones',$this->observaciones,true);
		$criteria->compare('diagnosticado',$this->diagnosticado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Evolucion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
