<?php

/**
 * This is the model class for table "paciente".
 *
 * The followings are the available columns in table 'paciente':
 * @property integer $id
 * @property string $primer_nombre
 * @property string $segundo_nombre
 * @property string $primer_apellido
 * @property string $segundo_apellido
 * @property string $nregistro_civil
 * @property string $fecha_nacimiento
 * @property string $ciudad
 * @property string $direccion
 * @property string $rh
 * @property string $sexo
 * @property string $ncert_nacido_vivo
 *
 * The followings are the available model relations:
 * @property CitaMedica[] $citaMedicas
 * @property Evolucion[] $evolucions
 * @property Acudiente[] $acudientes
 * @property VacunaPaciente[] $vacunaPacientes
 */
class Paciente extends CActiveRecord
{
	public $nombre;
	public $a_tpdocumento;
	public $a_documento;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'paciente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('primer_nombre, primer_apellido, segundo_apellido, fecha_nacimiento, ciudad, direccion, rh, sexo, ncert_nacido_vivo', 'required','message'=>'{attribute} no puede estar en blanco.'),
			array('primer_nombre, segundo_nombre, primer_apellido, segundo_apellido', 'length', 'max'=>10),
			array('nregistro_civil', 'length', 'max'=>20),
			array('ciudad, direccion', 'length', 'max'=>50),
			array('rh', 'length', 'max'=>3),
			array('sexo', 'length', 'max'=>1),
			array('ncert_nacido_vivo', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, nregistro_civil, fecha_nacimiento, ciudad, direccion, rh, sexo, ncert_nacido_vivo, nombre, a_tpdocumento, a_documento', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'citaMedicas' => array(self::HAS_MANY, 'CitaMedica', 'paciente_id'),
			'citaPendientes' => array(self::HAS_MANY, 'CitaMedica', 'paciente_id','condition'=>'estado=0 AND fechahora>now()'),
			'evolucions' => array(self::HAS_MANY, 'Evolucion', 'paciente_id'),
			'acudientes' => array(self::MANY_MANY, 'Acudiente', 'paciente_acudiente(paciente_id, acudiente_id)'),
			'vacunaPacientes' => array(self::HAS_MANY, 'VacunaPaciente', 'paciente_id'),
			'lastEvolucion'=>array(self::HAS_ONE,'Evolucion','paciente_id','order'=>'id DESC'),
			'escalaDesarrolloResultados' => array(self::HAS_MANY, 'EscalaDesarrolloResultado', 'paciente_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'primer_nombre' => 'Primer Nombre',
			'segundo_nombre' => 'Segundo Nombre',
			'primer_apellido' => 'Primer Apellido',
			'segundo_apellido' => 'Segundo Apellido',
			'nregistro_civil' => 'Número de Registro Civil',
			'fecha_nacimiento' => 'Fecha Nacimiento',
			'ciudad' => 'Ciudad',
			'direccion' => 'Dirección',
			'rh' => 'Grupo sanguineo y RH',
			'sexo' => 'Sexo',
			'ncert_nacido_vivo' => 'Número Certificado Nacido Vivo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->together = true;
		$criteria->with = array('acudientes'=>array('alias'=>'ac'));
		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.primer_nombre',$this->primer_nombre,true);
		$criteria->compare('t.segundo_nombre',$this->segundo_nombre,true);
		$criteria->compare('t.primer_apellido',$this->primer_apellido,true);
		$criteria->compare('t.segundo_apellido',$this->segundo_apellido,true);
		$criteria->compare('t.nregistro_civil',$this->nregistro_civil);
		$criteria->compare('t.fecha_nacimiento',$this->fecha_nacimiento);
		$criteria->compare('t.ciudad',$this->ciudad);
		$criteria->compare('t.direccion',$this->direccion,true);
		$criteria->compare('t.rh',$this->rh,true);
		$criteria->compare('t.sexo',$this->sexo,true);
		$criteria->compare('t.ncert_nacido_vivo',$this->ncert_nacido_vivo);
		$criteria->compare('ac.tipo_documento',$this->a_tpdocumento);
		$criteria->compare('ac.documento',$this->a_documento);
		$criteria->addCondition('replace(concat(t.primer_nombre," ",t.segundo_nombre," ",t.primer_apellido," ",t.segundo_apellido),"  "," ") LIKE "%'.$this->nombre.'%"');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Paciente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getNombres(){
		return CHtml::encode(trim($this->primer_nombre." ".$this->segundo_nombre));
	}

	public function getApellidos(){
		return CHtml::encode(trim($this->primer_apellido." ".$this->segundo_apellido));
	}

	public function getNombreCompleto(){
		return $this->getNombres()." ".$this->getApellidos();
	}

	public static function getABO(){
		return array(
			'O-'=>'O-',
			'O+'=>'O+',
			'A-'=>'A-',
			'A+'=>'A+',
			'B-'=>'B-',
			'B+'=>'B+',
			'AB-'=>'AB-',
			'AB+'=>'AB+',
			);
	}

	public function getUltimaEvolucion(){
		$evoluciones = $this->evolucions;
		return end($evoluciones);
	}

	public function getProximoControl(){
		$cpaciente = $this->evolucions;
		$cpaciente = end($cpaciente);
		$cpaciente = $cpaciente->evaluacionControl->control;

		$proximo = Control::model()->find(array('order'=>'tiempo','condition'=>'tiempo > :tiempo','params'=>array(':tiempo'=>$cpaciente->tiempo)));
		return $proximo;
	}

	public function getEvaluacionActual(){
		return $this->lastEvolucion->evaluacionControl->evaluacion;		

	}

	function verificaFinEvolucion($id){
		$data = $this->evolucions;
		foreach ($data as $key => $value) {
			if($value->evaluacion_control_id==$id) return true;
		}

		return false;
	}

	public function getControles(){
		// header('content-type: text/plain');
		$controles = $this->getEvaluacionActual()->evaluacionControls;
		$ret = array();
		foreach ($controles as $key => $value) {
			$control = $value->control; 
			// print_r($evoluciones);die;
			$ret[] = array(
				'id'=>$control->id,
				'titulo'=>$control->titulo,
				'status'=>$this->verificaFinEvolucion($value->id)?'OK':'NO',
				'penidnete'=>false
				);
		}

		return $ret;

	}

	public function getEdadEnMeses(){
		return floor((time()-strtotime($this->fecha_nacimiento))/((60*60*24*365)/12));
	}
}
