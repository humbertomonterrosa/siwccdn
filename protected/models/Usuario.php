<?php

/**
 * This is the model class for table "usuario".
 *
 * The followings are the available columns in table 'usuario':
 * @property integer $id
 * @property string $primer_nombre
 * @property string $segundo_nombre
 * @property string $primer_apellido
 * @property string $segundo_apellido
 * @property integer $tipo_documento
 * @property string $documento
 * @property string $username
 * @property string $password
 * @property string $fecha_creacion
 * @property integer $estado
 * @property integer $rol
 *
 * The followings are the available model relations:
 * @property Bitacora[] $bitacoras
 * @property CitaMedica[] $citaMedicas
 * @property CitaMedica[] $citaMedicas1
 * @property Evolucion[] $evolucions
 * @property TipoDocumento $tipoDocumento
 */
class Usuario extends CActiveRecord
{
	private static $current;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('primer_nombre, primer_apellido, segundo_apellido, tipo_documento, documento, username, password, fecha_creacion, estado', 'required'),
			array('tipo_documento, estado, rol', 'numerical', 'integerOnly'=>true),
			array('primer_nombre, segundo_nombre, primer_apellido, segundo_apellido', 'length', 'max'=>10),
			array('documento', 'length', 'max'=>15),
			array('username', 'length', 'max'=>16),
			array('username', 'unique','allowEmpty'=>false),
			array('password, fecha_creacion', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, tipo_documento, documento, username, password, fecha_creacion, estado, rol', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bitacoras' => array(self::HAS_MANY, 'Bitacora', 'usuario_id'),
			'citaMedicas' => array(self::HAS_MANY, 'CitaMedica', 'medico_id'),
			'citaMedicas1' => array(self::HAS_MANY, 'CitaMedica', 'usuario_id'),
			'evolucions' => array(self::HAS_MANY, 'Evolucion', 'diagnosticado'),
			'tipoDocumento' => array(self::BELONGS_TO, 'TipoDocumento', 'tipo_documento'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'primer_nombre' => 'Primer Nombre',
			'segundo_nombre' => 'Segundo Nombre',
			'primer_apellido' => 'Primer Apellido',
			'segundo_apellido' => 'Segundo Apellido',
			'tipo_documento' => 'Tipo Documento',
			'documento' => 'Documento',
			'username' => 'Username',
			'password' => 'Password',
			'fecha_creacion' => 'Fecha Creacion',
			'estado' => 'Estado',
			'rol' => 'Rol',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('primer_nombre',$this->primer_nombre,true);
		$criteria->compare('segundo_nombre',$this->segundo_nombre,true);
		$criteria->compare('primer_apellido',$this->primer_apellido,true);
		$criteria->compare('segundo_apellido',$this->segundo_apellido,true);
		$criteria->compare('tipo_documento',$this->tipo_documento);
		$criteria->compare('documento',$this->documento,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('fecha_creacion',$this->fecha_creacion,true);
		$criteria->compare('estado',$this->estado);
		$criteria->compare('rol',$this->rol);
		$criteria->addCondition('username != "superadmin"');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getNombres(){
		return CHtml::encode(trim($this->primer_nombre." ".$this->segundo_nombre));
	}

	public function getApellidos(){
		return CHtml::encode(trim($this->primer_apellido." ".$this->segundo_apellido));
	}

	public function getNombreCompleto(){
		return $this->getNombres()." ".$this->getApellidos();
	}

	public function getNombreCompletoCargo(){
		return $this->getNombreCompleto()." (".($this->rol=='1'?'Medico':'Enfermero(a)').")";
	}

	public function getEstadoText(){
		if((int)$this->estado===1) return "Activo";
		return "Inactivo";
	}

	public function getRolText(){
		switch ((int)$this->rol) {
			case 1: return "Medico"; break;
			case 2: return "Enfermero(a)"; break;
			default:
				return "Unknow";
				break;
		}
	}
}
