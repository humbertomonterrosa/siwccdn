<?php

/**
 * This is the model class for table "vacuna_edad".
 *
 * The followings are the available columns in table 'vacuna_edad':
 * @property integer $id
 * @property integer $vacuna_id
 * @property integer $edad_id
 * @property string $dosis
 *
 * The followings are the available model relations:
 * @property Edad $edad
 * @property Vacuna $vacuna
 * @property VacunaPaciente[] $vacunaPacientes
 */
class VacunaEdad extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vacuna_edad';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vacuna_id, edad_id, dosis', 'required'),
			array('vacuna_id, edad_id', 'numerical', 'integerOnly'=>true),
			array('dosis', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, vacuna_id, edad_id, dosis', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'edad' => array(self::BELONGS_TO, 'Edad', 'edad_id'),
			'vacuna' => array(self::BELONGS_TO, 'Vacuna', 'vacuna_id'),
			'vacunaPacientes' => array(self::HAS_MANY, 'VacunaPaciente', 'vacuna'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'vacuna_id' => 'Vacuna',
			'edad_id' => 'Edad',
			'dosis' => 'Dosis',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vacuna_id',$this->vacuna_id);
		$criteria->compare('edad_id',$this->edad_id);
		$criteria->compare('dosis',$this->dosis,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VacunaEdad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
