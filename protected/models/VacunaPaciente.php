<?php

/**
 * This is the model class for table "vacuna_paciente".
 *
 * The followings are the available columns in table 'vacuna_paciente':
 * @property integer $id
 * @property integer $paciente_id
 * @property integer $vacuna
 * @property string $fecha_aplicacion
 * @property string $laboratorio
 * @property string $numero_lote
 * @property string $ips_vacunadora
 * @property string $fecha_proxima
 * @property string $vacunador
 *
 * The followings are the available model relations:
 * @property Paciente $paciente
 * @property VacunaEdad $vacuna0
 */
class VacunaPaciente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vacuna_paciente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('paciente_id, vacuna, fecha_aplicacion, laboratorio, numero_lote, ips_vacunadora, vacunador', 'required'),
			array('paciente_id, vacuna', 'numerical', 'integerOnly'=>true),
			array('laboratorio, ips_vacunadora, vacunador', 'length', 'max'=>50),
			array('numero_lote', 'length', 'max'=>20),
			array('fecha_proxima', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, paciente_id, vacuna, fecha_aplicacion, laboratorio, numero_lote, ips_vacunadora, fecha_proxima, vacunador', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'paciente' => array(self::BELONGS_TO, 'Paciente', 'paciente_id'),
			'vacunaedad' => array(self::BELONGS_TO, 'VacunaEdad', 'vacuna'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'paciente_id' => 'Paciente',
			'vacuna' => 'Vacuna',
			'fecha_aplicacion' => 'Fecha Aplicación',
			'laboratorio' => 'Laboratorio',
			'numero_lote' => 'Número Lote',
			'ips_vacunadora' => 'IPS Vacunadora',
			'fecha_proxima' => 'Fecha Proxima Dosis',
			'vacunador' => 'Vacunador',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('paciente_id',$this->paciente_id);
		$criteria->compare('vacuna',$this->vacuna);
		$criteria->compare('fecha_aplicacion',$this->fecha_aplicacion,true);
		$criteria->compare('laboratorio',$this->laboratorio,true);
		$criteria->compare('numero_lote',$this->numero_lote,true);
		$criteria->compare('ips_vacunadora',$this->ips_vacunadora,true);
		$criteria->compare('fecha_proxima',$this->fecha_proxima,true);
		$criteria->compare('vacunador',$this->vacunador,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VacunaPaciente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
