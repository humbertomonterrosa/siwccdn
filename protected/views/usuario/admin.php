<div class="page-title">
	<div class="title_left">
		<h3>Administrador de usuarios</h3>
	</div>
	<div class="title_right">
        <a href="<?php echo $this->createUrl('create'); ?>" class="btn btn-success pull-right" style="margin-right:20px"><i class="glyphicon glyphicon-plus"></i> Crear Usuario</a>
	</div>
</div>
<div style="clear: both"></div>
<?php if(Yii::app()->user->hasFlash('error')): ?>
<div class="alert alert-danger alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php endif ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
<div class="alert alert-success alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php endif ?>

<div class="x_panel tile">
	<div class="x_content">
	<?php $this->widget('booster.widgets.TbGridView', array(
		'id'=>'usuario-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'name'=>'primer_nombre',
				'value'=>'$data->getNombreCompleto()',
				'header'=>'Nombre'
			),
			// 'tipo_documento',
			'documento',
			'username',
			// 'password',
			// 'fecha_creacion',
			array(
				'name'=>'estado',
				'value'=>'$data->getEstadoText()'
				),
			array(
				'name'=>'rol',
				'value'=>'$data->getRolText()'
				),
			
			array(
				'class'=>'CButtonColumn',
				'template'=>'{view} {update}'
			),
		),
	)); ?>
	</div>
</div>
