<?php
/* @var $this UsuarioController */
/* @var $model Usuario */

$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	$model->id,
);

?>

<div class="page-title">
	<div class="title_left">
		<h3><?php echo $model->getNombreCompleto(); ?></h3>
	</div>
</div>
<div style="clear: both"></div>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'primer_nombre',
		'segundo_nombre',
		'primer_apellido',
		'segundo_apellido',
		array(
			'name'=>'tipo_documento',
			'value'=>$model->tipoDocumento->titulo
			),
		'documento',
		'username',
		'fecha_creacion',
		array(
			'name'=>'estado',
			'value'=>$model->getEstadoText()
			),
		array(
			'name'=>'rol',
			'value'=>$model->getRolText()
			)
	),
)); ?>
