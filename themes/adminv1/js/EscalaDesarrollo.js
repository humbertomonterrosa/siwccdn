// Escala Desarrollo Module
!(function(scope){

	var _escalas = {};
	var _referencia = {};

	var fn = {
		//Autoload items Fn
		autoloadItems: function(EscalaId){
			var escala = _escalas['ED_'+EscalaId];		
			$('.row_ED','#table_ED_'+EscalaId).each(function(i,e){			
				var valor = $(e).attr('data-valor');
				var descripcion = $(e).attr('data-descripcion');
				var itemid = $(e).attr('data-item-id');

				var container = $('.group-button-container','#table_ED_'+EscalaId)[escala.cont++];			
				var buttons = $('<div class="btn-group">'+
									'<button type="button" class="btn btn-success btn-xs yes-btn"><i class="glyphicon glyphicon-ok"></i> Si</button>'+
									'<button type="button" class="btn btn-danger btn-xs not-btn"><i class="glyphicon glyphicon-remove"></i> No</button>'+
								'</div>');

				$('.yes-btn',buttons).on('click',function(){
					escala.currentItemId = itemid;
					escala.resultado = valor;
					escala.evalObj.html(valor);
					escala.evalCaption.html(fn.getReferenciaCaption(valor));
					buttons.remove();				
					$(container).html('Si');

					if(i<escala.items.length-1){
						nextcontainer = $('.group-button-container','#table_ED_'+EscalaId)[i+1];
						$(escala.items[i+1]).appendTo(nextcontainer);
					}
					else{
						$('.nav-tabs > .active').next('li').find('a').trigger('click');
					}

				});

				$('.not-btn',buttons).on('click',function(){
					if(escala.resultado==-1){
						escala.resultado = 0;
						escala.evalObj.html(0);
					}

					escala.evalCaption.html(fn.getReferenciaCaption(escala.resultado));

					buttons.remove()
					$(container).html('No');
					$('.nav-tabs > .active').next('li').find('a').trigger('click');
				});

				escala.items.push(buttons);

				if((escala.resultado == 0 && i == 0))
					$(buttons).appendTo(container);
				else{
					if(escala.resultado+1 == i)
						$(buttons).appendTo(container);	
				}
				
			});
		},

		//Get Referencia Caption
		getReferenciaCaption: function(value){
			var r = _referencia;
			if(value>=r.alerta_min&&value<=r.alerta_max) 											return "Alerta";
			else if(value>=r.medio_min&&value<=r.medio_max)											return "Medio";
			else if(value>=r.medio_alto_min&&(r.medio_alto_max == null || value<=r.medio_alto_max))	return "Medio Alto";
			else if(value>=r.alto_min) 																return "Alto";
			else 																					return "Valor invalido";
		},

		// Genera los datos que se envian por post
		getFormData: function(){
			var index = 0;
			var data = "";
			for(e in _escalas){
				if(index) data +="&";
				data += 'ResultadosForm[item][]='+_escalas[e].currentItemId;
				index++;
			}

			data += '&ResultadosForm[observaciones]='+$('#observaciones').val();
			data += '&ajax=1';
			data += '&'+csrfToken.name+'='+csrfToken.value;

			return data;

		},

		//Validar form
		validateFormFields: function(){
			for(e in _escalas){
				if(_escalas[e].currentItemId==null){
					return false;
				}
			}			

			return true;
		}
	};

	scope.EscalaDesarrollo = {
		addReferencia: function(attributes){
			_referencia = attributes;
		},	
		addEscala: function(id,titulo,inicial=null){
			var evalObj = $('.eval_value','#eval_ED_'+id);	
			var evalCaption = $('.text_caption','#eval_ED_'+id);
			_escalas['ED_'+id] = {id:id,titulo:titulo,resultado:inicial==null?-1:inicial,currentItemId:null,cont:0,items:[],evalObj:evalObj,evalCaption:evalCaption};

			if(inicial!=null){
				evalObj.html(inicial);				
			}

			//Autoload items
			fn.autoloadItems(id);		
		}
	};

	$(document).on('click','.escala-desarrollo-btn-finish',function(){
		var action = $(this).attr('href');
		if(fn.validateFormFields()){
			$.ajax({
				url:'',
				type:'POST',
				dataType:'json',
				data:fn.getFormData(),
				success:function(data){
					if(!data.error){
						alert(data.message);
						location.reload();
					}
					else{
						alert(data.message);
					}
				},
				error: function(a,b,error){
					alert('Se produjo un error al enviar los resultados de la evaluación');
				}
			});
		}
		else{
			alert('Verifique que todas las escalas de desarrollo esten evaluadas');
		}

		return false;
	});

})(window);

