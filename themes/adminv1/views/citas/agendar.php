<div class="page-title">
	<div class="title_left">
		<h3>Agendamiento de citas</h3>
	</div>
</div>
<div class="clearfix"></div>
<?php if(Yii::app()->user->hasFlash('error')): ?>
<div class="alert alert-danger alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php endif ?>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					<li class="fa fa-calendar"></li> Agendar cita de control
				</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="x_title">Información del paciente</div>
				<div class="row">
					<div class="col-md-4">
						<div>
							<strong>Nombre:</strong>
						</div>
						<div><?php echo $paciente->getNombreCompleto(); ?></div>
					</div>
					<div class="col-md-4">
						<div>
							<strong>Fecha nacimiento:</strong>
						</div>
						<div><?php echo date("d/m/Y",strtotime($paciente->fecha_nacimiento)); ?></div>
					</div>
					<div class="col-md-4">
						<div>
							<strong>Grupo Sanguineo y RH:</strong>
						</div>
						<div><?php echo $paciente->rh; ?></div>
					</div>
					<div class="clearfix"></div>
					<div class="col-md-4">
						<div>
							<strong>Número de registro Civil</strong>
						</div>
						<div><?php echo $paciente->nregistro_civil; ?></div>
					</div>
					<div class="col-md-4">
						<div>
							<strong>Dirección:</strong>
						</div>
						<div><?php echo CHtml::encode($paciente->direccion); ?></div>
					</div>
					<div class="col-md-4">
						<div>
							<strong>Ciudad:</strong>
						</div>
						<div><?php echo CHtml::encode($paciente->ciudad); ?></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<br>
				<?php if(isset($cita)): ?>
					<div class="alert alert-info">
						EL PACIENTE YA CUENTA CON UNA CITA DE CONTROL PROGRAMADA.
					</div>
					<div class="x_panel">
						<div class="x_content">
							<div class="row">
								<div style="padding:5px 0px">
									<div class="col-md-3"><strong>Medico/Enfermero(a)</strong></div>
									<div class="col-md-3"><?php echo $cita->medico->getNombreCompleto() ?></div>
									<div class="col-md-3"><strong>Fecha:</strong></div>
									<div class="col-md-3"><?php echo $cita->fechahora; ?></div>
									<div class="clearfix"></div>
								</div>
								<div style="padding:5px 0px">
									<div class="col-md-3"><strong>Motivo:</strong></div>
									<div class="col-md-9"><?php echo CHtml::encode($cita->motivo); ?></div>
									<div class="clearfix"></div>
								</div>
								<div style="padding:5px 0px">
									<div class="col-md-3"><strong>Asignada por:</strong></div>
									<div class="col-md-9"><?php echo CHtml::encode($cita->usuario->getNombreCompleto()); ?></div>
									<div class="clearfix"></div>
								</div>
							</div>
							<a href="<?php echo $this->createUrl('/citas/cancelar/'.$cita->paciente_id) ?>" class="btn btn-danger">Cancelar cita</a>
							<a href="<?php echo $this->createUrl('/citas/imprimir/'.$cita->paciente_id) ?>" target="_blank" class="btn btn-info"><i class="glyphicon glyphicon-print"></i> Imprimir cita</a>
						</div>
					</div>
				<?php else: ?>
					<?php $proximo = $paciente->getProximoControl(); ?>
					<?php $rangoa = date('Y-m-d',strtotime("+".$proximo->inicio." days",strtotime($paciente->fecha_nacimiento))); ?>
					<?php $rangob = date('Y-m-d',strtotime("+".$proximo->tiempo." days",strtotime($paciente->fecha_nacimiento))); ?>
					<br>
					<?php $form = $this->beginWidget('CActiveForm',array(
						'id'=>'agendar-cita'
					)); ?>
					
					<div class="form-group">
						<?php echo $form->labelEx($model,'medico_id'); ?>
						<?php echo $form->dropDownList($model,'medico_id',CHtml::listData($medico,'id','nombreCompletoCargo'),array('empty'=>'Seleccione medico','class'=>'form-control')); ?>
						<?php echo $form->error($model,'medico_id'); ?>
					</div>

					<div class="form-group">
						<?php echo $form->labelEx($model,'fecha'); ?>
						<?php echo $form->dateField($model,'fecha',array('class'=>'form-control','min'=>$rangoa,'max'=>$rangob)); ?>
						<?php echo $form->error($model,'fecha'); ?>
					</div>

					<div class="form-group">
						<?php echo $form->labelEx($model,'hora'); ?>
						<?php echo $form->dropDownList($model,'hora',CitaMedica::getHoras(),array('class'=>'form-control')); ?>
						<?php echo $form->error($model,'hora'); ?>
					</div>

					<div class="form-group">
						<?php echo $form->labelEx($model,'motivo'); ?>
						<?php echo $form->textarea($model,'motivo',array('class'=>'form-control')); ?>
						<?php echo $form->error($model,'motivo'); ?>
					</div>
					<br />
					<?php echo CHtml::submitButton('Agendar cita',array('class'=>'btn btn-primary btn-block')) ?>

					<?php $this->endWidget(); ?>
				<?php endif ?>
			</div>
		</div>
	</div>
</div>