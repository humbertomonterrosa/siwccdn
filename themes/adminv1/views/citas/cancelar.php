<div class="x_panel">
	<div class="x_title">
		<h3>Cancelar cita</h3>
	</div>
	<div class="x_content">
		<p>Esta a punto de cancelar la cita de control programada para el día <?php echo $cita->fechahora; ?> con el <?php echo ($cita->medico->rol=='1'?'medico':'enfermero(a)').' '.$cita->medico->getNombreCompleto(); ?></p>
		<p>¿Está seguro que desea cancelar esta cita?</p>
		<?php $form = $this->beginWidget('CActiveForm'); ?>
			<input type="submit" name="confirm" value="Aceptar" class="btn btn-succes" />
			<a href="<?php echo $this->createUrl('/citas/agendar/'.$cita->paciente_id); ?>" class="btn btn-danger">Cancelar</a>
		<?php $this->endWidget(); ?>
	</div>
</div>
<?php if($cita): ?>
	
<?php else: ?>
	No existe citas para cancelar.
<?php endif; ?>