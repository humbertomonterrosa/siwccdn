<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<title>Cita Medica</title>
		<style type="text/css">
			body{
				font-family: sans-serif, Arial;
			}
			.container{
				border:1px #000 solid;
				padding:10px;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<table width="100%">
				<tr>
					<td width="0%">
						<h3 style="margin:0px"><?php echo Yii::app()->name; ?></h3>
						<div style="font-size: 13px">
							<div>Nit: xxx.xxx.xxx-x</div>
							<div>Calle 1 # 2 - 3</div>
							<div>Teléfono: 555 5555</div>
							<div>Cereté, Córdoba</div>				
						</div>					
					</td>
					<td>
						<b>Cita No:</b> <?php echo str_pad($cita->id, 10,"0",STR_PAD_LEFT); ?>
					</td>
				</tr>
			</table>			

			<table width="100%" style="margin-top:15px">
				<tr>
					<td width="50%"><b>Fecha de cita: <?php 
					$dia = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
					echo $dia[date('w',strtotime($cita->fechahora))].' '.date('d/m/Y',strtotime($cita->fechahora)); ?></b></td>
					<td><b>Hora: <?php echo date('h:i a',strtotime($cita->fechahora)); ?></b></td>
				</tr>
				<tr>
					<td style="padding: 10px 0px" colspan="2">Motivo de la cita</td>
				</tr>
			</table>

			<table width="100%" cellpadding="5">
				<tr>
					<td colspan="2" style="background:#CCCCCC; font-weight: bold">Prestador</td>
				</tr>
				<tr>
					<td width="150px" align="right">Identificación:</td>
					<td><?php echo $cita->medico->documento; ?></td>
				</tr>
				<tr>
					<td align="right">Nombre:</td>
					<td><b><?php echo $cita->medico->getNombreCompleto(); ?></b></td>
				</tr>
				<tr>
					<td colspan="2" style="background:#CCCCCC; font-weight: bold">Paciente</td>
				</tr>
				<tr>
					<td align="right">No. de registro civil:</td>
					<td><?php echo $paciente->nregistro_civil; ?></td>
				</tr>
				<tr>
					<td align="right">Nombre:</td>
					<td><?php echo $paciente->getNombreCompleto(); ?></td>
				</tr>
				<tr>
					<td align="right">Sexo:</td>
					<td><?php echo $paciente->sexo=='M'?'MASCULINO':'FEMENINO'; ?></td>
				</tr>
				<tr>
					<td align="right">Edad:</td>
					<td><?php echo ($meses = $paciente->getEdadEnMeses())." Mes".($meses!=1?'es':''); ?></td>
				</tr>
			</table>			
		</div>
		<script>
			window.print();
		</script>
	</body>
</html>