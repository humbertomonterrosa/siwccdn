<div class="page-title">
	<div class="title_left">
		<h3>Agenda de citas</h3>
	</div>	
</div>
<div style="clear: both"></div>
<?php if(Yii::app()->user->hasFlash('error')): ?>
<div class="alert alert-danger alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php endif ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
<div class="alert alert-success alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php endif ?>

<div id="citas" style="width: 750px; margin:0 auto">
	
</div>


<?php 
$cs = Yii::app()->clientScript;
$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/calendar/fullcalendar.css');
$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/calendar/fullcalendar.min.js',CClientScript::POS_END);
$cs->registerScript('calendar','
    $("#citas").fullCalendar({
        header: {
            left:"prev,next today",
            center: "title",
            right: "month,agendaWeek,agendaDay"
        },                
        defaultView:"agendaWeek",
        defaultDate: "'.date("Y-m-d").'",
        eventLimit: true,
        events: '.$events.'
    })',CClientScript::POS_END);

 ?>