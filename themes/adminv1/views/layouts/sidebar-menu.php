<?php 
  $menu = array(
    'sites'=>array('title'=>'Inicio','icon'=>'fa fa-home','url'=>'/site'),
    'paciente'=>array('title'=>'Pacientes','icon'=>'fa fa-users','url'=>'/paciente'),
    'Citas'=>array('title'=>'Citas','icon'=>'fa fa-calendar','url'=>'/citas'),
    'Usuarios'=>array('title'=>'Usuarios','icon'=>'fa fa-user','url'=>'/usuario','user'=>'superadmin')

  ); 
?>                    
                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            <h3>General</h3>
                            <ul class="nav side-menu">
                                <?php foreach ($menu as $key => $value):?>
                                    <?php 
                                        if(isset($value['user'])){
                                            if(Yii::app()->user->id!=$value['user']) continue;
                                        }
                                     ?>
                                <li <?php echo $this->tab==$key?'class="current-page"':''; ?>>
                                    <a href="<?php echo $this->createUrl($value['url']); ?>"><i class="<?php echo $value['icon']; ?>"></i> <?php echo $value['title']; ?></a>
                                </li>
                            <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->