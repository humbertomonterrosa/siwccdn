<?php
/* @var $this PacienteController */
/* @var $model Paciente */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'paciente-create-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<div class="x_panel tile">
		<div class="x_title">
			<h2>Registro de paciente</h2>
			<div class="pull-right">
				<?php echo CHtml::link('Cancelar',$this->createUrl('index'),array('confirm'=>'¿Desea cancelar la creación del paciente?','class'=>'btn btn-danger btn-sm btn-flat')) ?>
				<button type="submit" class="btn btn-success btn-sm btn-flat"><li class="fa fa-check"></li> Guardar paciente</button>
			</div>
			<div class="clearfix"></div>
		</div>
		
		<div class="x_content">
			<p class="note">Los campos con <span class="required">*</span> son obligatorios.</p>
			

			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="x_title">DATOS DEL PACIENTE</div>

					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6">
							<div class="form-group">
								<?php echo $form->labelEx($model,'primer_nombre'); ?>
								<?php echo $form->textField($model,'primer_nombre',array('class'=>'form-control')); ?>
								<?php echo $form->error($model,'primer_nombre',array('class'=>'text-danger')); ?>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<div class="form-group">
								<?php echo $form->labelEx($model,'segundo_nombre'); ?>
								<?php echo $form->textField($model,'segundo_nombre',array('class'=>'form-control')); ?>
								<?php echo $form->error($model,'segundo_nombre',array('class'=>'text-danger')); ?>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<div class="form-group">
								<?php echo $form->labelEx($model,'primer_apellido'); ?>
								<?php echo $form->textField($model,'primer_apellido',array('class'=>'form-control')); ?>
								<?php echo $form->error($model,'primer_apellido',array('class'=>'text-danger')); ?>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<div class="form-group">
								<?php echo $form->labelEx($model,'segundo_apellido'); ?>
								<?php echo $form->textField($model,'segundo_apellido',array('class'=>'form-control')); ?>
								<?php echo $form->error($model,'segundo_apellido',array('class'=>'text-danger')); ?>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<?php echo $form->labelEx($model,'nregistro_civil'); ?>
								<?php echo $form->textField($model,'nregistro_civil',array('class'=>'form-control')); ?>
								<?php echo $form->error($model,'nregistro_civil',array('class'=>'text-danger')); ?>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<?php echo $form->labelEx($model,'ncert_nacido_vivo'); ?>
								<?php echo $form->textField($model,'ncert_nacido_vivo',array('class'=>'form-control')); ?>
								<?php echo $form->error($model,'ncert_nacido_vivo',array('class'=>'text-danger')); ?>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<?php echo $form->labelEx($model,'fecha_nacimiento'); ?>
								<?php echo $form->dateField($model,'fecha_nacimiento',array('class'=>'form-control')); ?>
								<?php echo $form->error($model,'fecha_nacimiento',array('class'=>'text-danger')); ?>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<div class="form-group">
								<?php echo $form->labelEx($model,'direccion'); ?>
								<?php echo $form->textField($model,'direccion',array('class'=>'form-control')); ?>
								<?php echo $form->error($model,'direccion',array('class'=>'text-danger')); ?>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<div class="form-group">
								<?php echo $form->labelEx($model,'ciudad'); ?>
								<?php echo $form->textField($model,'ciudad',array('class'=>'form-control')); ?>
								<?php echo $form->error($model,'ciudad',array('class'=>'text-danger')); ?>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<div class="form-group">
								<?php echo $form->labelEx($model,'rh'); ?>
								<?php echo $form->dropDownList($model,'rh',Paciente::getABO(),array('empty'=>'...','class'=>'form-control')); ?>
								<?php echo $form->error($model,'rh',array('class'=>'text-danger')); ?>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<div class="form-group">
								<?php echo $form->labelEx($model,'sexo'); ?>
								<?php echo $form->dropDownList($model,'sexo',array('F'=>'FEMENINO','M'=>'MASCULINO'),array('empty'=>'...','class'=>'form-control')); ?>
								<?php echo $form->error($model,'sexo',array('class'=>'text-danger')); ?>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-4 col-sm-4 col-xs-4">
							<div class="form-group">
								<?php echo $form->labelEx($evolucion,'peso'); ?>
								<?php echo $form->textField($evolucion,'peso',array('class'=>'form-control')); ?>
								<?php echo $form->error($evolucion,'peso',array('class'=>'text-danger')); ?>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4">
							<div class="form-group">
								<?php echo $form->labelEx($evolucion,'talla'); ?>
								<?php echo $form->textField($evolucion,'talla',array('class'=>'form-control')); ?>
								<?php echo $form->error($evolucion,'talla',array('class'=>'text-danger')); ?>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4">
							<div class="form-group">
								<?php echo $form->labelEx($evolucion,'perimetro_cefalico'); ?>
								<?php echo $form->textField($evolucion,'perimetro_cefalico',array('class'=>'form-control')); ?>
								<?php echo $form->error($evolucion,'perimetro_cefalico',array('class'=>'text-danger')); ?>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<?php echo $form->labelEx($evolucion,'observaciones'); ?>
								<?php echo $form->textArea($evolucion,'observaciones',array('class'=>'form-control')); ?>
								<?php echo $form->error($evolucion,'observaciones',array('class'=>'text-danger')); ?>
							</div>
						</div>

					</div>

				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="x_title">ACUDIENTES</div>
					<?php foreach ($acudientes as $key => $value): ?>
					<div class="x_panel tile">
						<div class="x_title">
							<h2>Acudiente #<?php echo $key+1; ?></h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<?php echo $form->labelEx($value,'parentesco'); ?>
										<?php echo $form->dropDownList($value,'parentesco',Acudiente::getParentescos(),array('empty'=>'...','class'=>'form-control','name'=>'Acudientes['.$key.'][parentesco]')); ?>
										<?php echo $form->error($value,'parentesco',array('class'=>'text-danger')); ?>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6">
									<div class="form-group">
										<?php echo $form->labelEx($value,'primer_nombre'); ?>
										<?php echo $form->textField($value,'primer_nombre',array('class'=>'form-control','name'=>'Acudientes['.$key.'][primer_nombre]')); ?>
										<?php echo $form->error($value,'primer_nombre',array('class'=>'text-danger')); ?>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6">
									<div class="form-group">
										<?php echo $form->labelEx($value,'segundo_nombre'); ?>
										<?php echo $form->textField($value,'segundo_nombre',array('class'=>'form-control','name'=>'Acudientes['.$key.'][segundo_nombre]')); ?>
										<?php echo $form->error($value,'segundo_nombre',array('class'=>'text-danger')); ?>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-6 col-sm-6 col-xs-6">
									<div class="form-group">
										<?php echo $form->labelEx($value,'primer_apellido'); ?>
										<?php echo $form->textField($value,'primer_apellido',array('class'=>'form-control','name'=>'Acudientes['.$key.'][primer_apellido]')); ?>
										<?php echo $form->error($value,'primer_apellido',array('class'=>'text-danger')); ?>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6">
									<div class="form-group">
										<?php echo $form->labelEx($value,'segundo_apellido'); ?>
										<?php echo $form->textField($value,'segundo_apellido',array('class'=>'form-control','name'=>'Acudientes['.$key.'][segundo_apellido]')); ?>
										<?php echo $form->error($value,'segundo_apellido',array('class'=>'text-danger')); ?>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-6 col-sm-6 col-xs-6">
									<div class="form-group">
										<?php echo $form->labelEx($value,'tipo_documento'); ?>
										<?php echo $form->dropDownList($value,'tipo_documento',CHtml::listData(TipoDocumento::model()->findAll(),'id','titulo'),array('empty'=>'...','class'=>'form-control','name'=>'Acudientes['.$key.'][tipo_documento]')); ?>
										<?php echo $form->error($value,'tipo_documento',array('class'=>'text-danger')); ?>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6">
									<div class="form-group">
										<?php echo $form->labelEx($value,'documento'); ?>
										<?php echo $form->textField($value,'documento',array('class'=>'form-control','name'=>'Acudientes['.$key.'][documento]')); ?>
										<?php echo $form->error($value,'documento',array('class'=>'text-danger')); ?>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-6 col-sm-6 col-xs-6">
									<div class="form-group">
										<?php echo $form->labelEx($value,'direccion'); ?>
										<?php echo $form->textField($value,'direccion',array('class'=>'form-control','name'=>'Acudientes['.$key.'][direccion]')); ?>
										<?php echo $form->error($value,'direccion',array('class'=>'text-danger')); ?>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6">
									<div class="form-group">
										<?php echo $form->labelEx($value,'ciudad'); ?>
										<?php echo $form->textField($value,'ciudad',array('class'=>'form-control','name'=>'Acudientes['.$key.'][ciudad]')); ?>
										<?php echo $form->error($value,'ciudad',array('class'=>'text-danger')); ?>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-6 col-sm-6 col-xs-6">
									<div class="form-group">
										<?php echo $form->labelEx($value,'telefono'); ?>
										<?php echo $form->textField($value,'telefono',array('class'=>'form-control','name'=>'Acudientes['.$key.'][telefono]')); ?>
										<?php echo $form->error($value,'telefono',array('class'=>'text-danger')); ?>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6">
									<div class="form-group">
										<?php echo $form->labelEx($value,'correo'); ?>
										<?php echo $form->textField($value,'correo',array('class'=>'form-control','name'=>'Acudientes['.$key.'][correo]')); ?>
										<?php echo $form->error($value,'correo',array('class'=>'text-danger')); ?>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>

					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>

<?php $this->endWidget(); ?>

