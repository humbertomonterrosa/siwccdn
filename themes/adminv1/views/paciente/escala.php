<div class="page-title">
	<div class="title_left">
		<h3>Escala de desarrollo</h3>
	</div>	
</div>
<div class="clearfix"></div>

<?php 

$this->widget('ext.yiibooster.widgets.TbBreadcrumbs',
	array(
		'homeLink'=>'Paciente',
		'links'=>array($model->getNombreCompleto()=>array('/paciente/view','id'=>$model->id),'Escala de desarrollo')
		)
	);

$res = array();
$reslist = $model->escalaDesarrolloResultados;
if($reslist){
	$items = end($reslist)->escalaDesarrolloResultadoItems;
	foreach ($items as $key => $value) {
		$res[$value->escalaDesarrolloItem->escalaDesarrollo->id] = $value->escalaDesarrolloItem->valor;
	}
}
?>

<?php if(Yii::app()->user->hasFlash('error')): ?>
<div class="alert alert-danger alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php endif ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
<div class="alert alert-success alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php endif ?>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">	
		<div class="x_panel">			
			<div class="x_content">
			<?php $edad = $model->getEdadEnMeses(); ?>
			<div><b>Grupo de Edad del Paciente:</b> <?php echo $grupo->grupo; ?> Meses</div>
			<div><b>Edad Actual del Paciente:</b> <?php echo $edad." Mes".($edad>1?"es":""); ?></div>
			<div class="row">
				<?php foreach ($escalas as $key => $value) : ?>
					<div id="eval_ED_<?php echo $value->id; ?>" style="background: #F4F4F4; border-radius:7px; border:1px #ccc solid; margin:10px; padding: 10px; font-size:11px; text-align: center;" class="col-md-<?php echo floor(12/count($escalas)-1); ?> col-sm-<?php echo floor(12/count($escalas)); ?> col-xs-<?php echo floor(12/count($escalas)); ?>">
						<div><?php echo $value->titulo; ?></div>
						<div class="eval_value" style="font-size: 30px; text-align: center; font-weight: bold; margin: 15px">?</div>
						<div class="text_caption" style="text-align:center">Sin definir</div>
					</div>
				<?php endforeach ?>
				<div class="col-md-3 col-sm-3">
					<textarea id="observaciones" placeholder="Observaciones" style="margin: 10px 0px; width:100%; height: 80px"></textarea>
					<a href="#" class="btn btn-success btn-block escala-desarrollo-btn-finish">Finalizar Evaluación</a>
				</div>
			</div>
			<?php 
				$tabs = array(
						array('label'=>'Resultados','content'=>$this->renderPartial('escala_resultado',array('model'=>$model,'escalas'=>$escalas),true),'active'=>true)
				);
				foreach ($escalas as $key => $value) {
					$tabs[] = array('label'=>$value->titulo,'content'=>$this->renderPartial('escala_tabla',array('model'=>$value,'init'=>isset($res[$value->id])?$res[$value->id]:-1),true));
				}
			 ?>
			<?php $this->widget('ext.yiibooster.widgets.TbTabs',
					array(
						'type'=>'tabs',
						'tabs'=>$tabs,
						'justified' => true
						)
					); ?>
			</div>
		</div>
	</div>
</div>
<?php 
	$csrfTokenName = Yii::app()->request->csrfTokenName;
	$csrfToken = Yii::app()->request->csrfToken;
	Yii::app()->clientScript->registerScript('csrf','window["csrfToken"] = {name:"'.$csrfTokenName.'",value:"'.$csrfToken.'"};'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl."/js/EscalaDesarrollo.js?_=".time()); ?>
<?php Yii::app()->clientScript->registerScript('loadreferences','EscalaDesarrollo.addReferencia('.json_encode($ref->attributes).');'); ?>

