<?php if(!$model->escalaDesarrolloResultados): ?>
	<div style="margin:30px; text-align: center">No se encontraron evaluaciones para este paciente.</div>
<?php else: ?>
	<table width="100%" class="table table-bordered mytable">
		<tr>
			<th width="170px">Fecha</th>
			<th width="75px">Edad</th>
			<?php foreach ($escalas as $key => $value): ?>
				<th width="60px"><?php echo $value->abreviatura; ?></th>
			<?php endforeach ?>
			<th width="60px">Total</th>
			<th>Observaciones</th>
		</tr>
	<?php foreach ($model->escalaDesarrolloResultados as $key => $value): ?>
		<tr>
			<td><?php echo $value->fecha; ?></td>
			<td style="text-align:center"><?php echo $value->edad." Mes".($value->edad!=1?'es':''); ?></td>
			<?php 
				$total = 0;
				$ed = array();
				foreach ($value->escalaDesarrolloResultadoItems as $k => $v) {
					$item = $v->escalaDesarrolloItem;
					$ed[$item->escalaDesarrollo->id] = $item->valor;
					$total += $item->valor;
				}
			?>
			<?php foreach ($escalas as $k => $v): ?>
				<td style="text-align:center" title="<?php echo $v->titulo; ?>"><?php echo $ed[$v->id]; ?></td>
			<?php endforeach ?>
			<td style="text-align:center"><?php echo $total; ?></td>
			<td><?php echo $value->observaciones; ?></td>
		</tr>
	<?php endforeach ?>
	</table>
<?php endif ?>