
<?php 
	$criteria = new CDbCriteria;
	$criteria->together = true;
	$criteria->with = array('escalaDesarrolloItems'=>array('alias'=>'edi'));
	$criteria->compare('edi.escala_desarrollo_id',$model->id);

?>
<table id="table_ED_<?php echo $model->id; ?>" class="table table-bordered">
<tr>
	<th width="15">Edades</th>
	<th width="15">Item</th>
	<th>Revisión</th>
	<th width="150"></th>
</tr>
<?php $edades = EscalaDesarrolloEdades::model()->findAll($criteria); ?>
	<?php foreach ($edades as $key => $edad): ?>
		<?php $rows = $edad->escalaDesarrolloItems; ?>
		<?php $numrows = count($rows); ?>
		<?php foreach ($rows as $k => $item): ?>
		<tr class="row_ED" data-descripcion="<?php echo $item->descripcion; ?>" data-valor="<?php echo $item->valor; ?>" data-item-id="<?php echo $item->id; ?>">
			<?php if(!$k): ?>
				<th style="vertical-align: middle; text-align: center;" rowspan="<?php echo $numrows; ?>"><?php echo $edad->grupo; ?></th>
			<?php endif ?>
			<th style="vertical-align: middle; text-align: center;"><?php echo $item->valor; ?></th>
			<td><?php echo $item->descripcion; ?></td>
			<td class="group-button-container">
				
			</td>
		</tr>
		<?php endforeach ?>
	<?php endforeach ?>
</table>

<?php Yii::app()->clientScript->registerScript('addItems_ED_'.$model->id,"EscalaDesarrollo.addEscala($model->id,'$model->titulo'".($init!=-1?','.$init:'').");",CClientScript::POS_READY); ?>

