<?php $form = $this->beginWidget('CActiveForm',array(
	'id'=>'busca-paciente',
	'enableClientValidation'=>false,
)); ?>
<div class="x_panel tile">
	<div class="x_title">
		<h2>Gestión de Pacientes</h2>
		<?php echo CHtml::link('<li class="fa fa-plus"></li> Nuevo paciente',$this->createUrl('create'),array('class'=>'btn btn-success btn-sm btn-flat pull-right')); ?>
		<div class="clearfix"></div>
	</div>
	<div class="x_content">
		<p>
			A continuación se presenta un formulario en el que puede hacer filtros para buscar 
			un paciente, puede hacerlo introduciendo información del paciente o puede optar por 
			buscar por la persona que tiene a cargo en menor.
		</p>

		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-6">
				<div class="x_title">INFORMACIÓN DEL PACIENTE</div>
				<div class="form-group">
					<?php echo $form->labelEx($model,'nombre'); ?>
					<?php echo $form->textField($model,'nombre',array('class'=>'form-control')); ?>
				</div>
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="form-group">
							<?php echo $form->labelEx($model,'registrocivil'); ?>
							<?php echo $form->textField($model,'registrocivil',array('class'=>'form-control')); ?>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="form-group">
							<?php echo $form->labelEx($model,'certnacidovivo'); ?>
							<?php echo $form->textField($model,'certnacidovivo',array('class'=>'form-control')); ?>
						</div>
					</div>
				</div>
					
					
					
			</div>
			<div class="col-md-6 col-sm-6 col-xs-6">
				<div class="x_title">INFORMACIÓN DEL ACUDIENTE</div>
				<div class="form-group">
					<?php echo $form->labelEx($model,'tpdocumento'); ?>
					<?php echo $form->dropDownList($model,'tpdocumento',CHtml::listData(TipoDocumento::model()->findAll(),'id','titulo'),array('empty'=>'...','class'=>'form-control')); ?>
				</div>
				<div class="form-group">
					<?php echo $form->labelEx($model,'documento'); ?>
					<?php echo $form->textField($model,'documento',array('class'=>'form-control')); ?>
					<?php echo $form->error($model,'documento'); ?>
				</div>				
			</div>
			<div class="clearfix"></div>
			<button class="btn btn-primary btn-sm btn-block"><li class="fa fa-search"></li> BÚSCAR PACIENTE</button>
		</div>
		<?php if(!is_null($data)): ?>
		<?php $this->widget('ext.yiibooster.widgets.TbGridView',array(
			'type' => 'striped bordered condensed',
			'dataProvider'=>$data->search(),
			'template'=>'{items}',
			'emptyText'=>'No se encontraron resultados para su consulta.',
			'columns'=>array(
				array(
					'header'=>'Nombre del Paciente',
					'type'=>'raw',
					'value'=>'CHtml::link($data->getNombreCompleto(),array("view","id"=>$data->id))'
					),
				array(
					'header'=>'Registro Civil',
					'type'=>'raw',
					'value'=>'$data->nregistro_civil==0?"Sin definir":$data->nregistro_civil'
					),
				array(
					'header'=>'Fecha de Nacimiento',
					'type'=>'raw',
					'value'=>'date("d/m/Y",strtotime($data->fecha_nacimiento))'
					),
				array(
					'header'=>'Ciudad',
					'type'=>'raw',
					'value'=>'CHtml::encode($data->ciudad)'
					)
				)
		)); ?>
		<?php endif; ?>
	</div>
</div>
<?php $this->endWidget(); ?>