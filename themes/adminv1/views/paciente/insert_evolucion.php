<?php $form = $this->beginWidget('CActiveForm',array(
	'id'=>'registro-evolucion',
	'enableClientValidation'=>false
	)); ?>
	<div class="modal-body">
		<div class="row">
			<div class="form-group">
					<label for="">Edad para el día de la muestra</label>
					<?php echo $form->dropDownList($evolucion,'edad',$model->getProximoControl()->getArrayRange()); ?>
					<?php echo $form->error($evolucion,'edad',array('class'=>'text-danger')); ?>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4">
				<div class="form-group">
					<?php echo $form->labelEx($evolucion,'peso'); ?>
					<?php echo $form->textField($evolucion,'peso',array('class'=>'form-control')); ?>
					<?php echo $form->error($evolucion,'peso',array('class'=>'text-danger')); ?>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4">
				<div class="form-group">
					<?php echo $form->labelEx($evolucion,'talla'); ?>
					<?php echo $form->textField($evolucion,'talla',array('class'=>'form-control')); ?>
					<?php echo $form->error($evolucion,'talla',array('class'=>'text-danger')); ?>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4">
				<div class="form-group">
					<?php echo $form->labelEx($evolucion,'perimetro_cefalico'); ?>
					<?php echo $form->textField($evolucion,'perimetro_cefalico',array('class'=>'form-control')); ?>
					<?php echo $form->error($evolucion,'perimetro_cefalico',array('class'=>'text-danger')); ?>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<?php echo $form->labelEx($evolucion,'observaciones'); ?>
					<?php echo $form->textArea($evolucion,'observaciones',array('class'=>'form-control')); ?>
					<?php echo $form->error($evolucion,'observaciones',array('class'=>'text-danger')); ?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary">Guardar evolución</button>
    </div>
<?php $this->endWidget(); ?>