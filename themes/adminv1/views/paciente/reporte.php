<!DOCTYPE html>
<html>
<head>
	<title>Reporte</title>
	<meta charset="utf-8">
	<style type="text/css">
		body{
			margin:0px;
			padding:0px;
			font-family: Arial,sans-serif;
			font-size: 15px;
		}

		.header{
			border-bottom:3px #000 solid;
			margin-bottom:2px; 
			padding:0px 10px;
		}

		.subheader{
			width:100%;
			border-top: 1px #000 solid;
		}

		table{
			border-collapse: collapse;
			width: 100%;
		}
		table table tr td div{
			min-height: 17px;
		}

	</style>
</head>
<body>
	<div class="header">
		<table>
			<tr>
				<td><h3><?php echo $model->getNombreCompleto(); ?></h3></td>
				<td style="text-align:right">Fecha generación: <strong><?php echo date('d/m/Y h:i a'); ?></strong></td>
			</tr>
		</table>
	</div>
	<div class="subheader"></div>
	<br />
	<!-- Informacion del paciente -->
	<table border="1" cellpadding="5">
		<tr>
			<td style="border:2px #000 solid"><strong>PACIENTE</strong></td>
		</tr>
		<tr>
			<td style="padding:15px">
				<table width="100%" cellpadding="5" cellspacing="5">
					<tr>
						<td width="50%">
							<div>
								<strong>Nombres</strong>
							</div>
							<div>
								<?php echo $model->getNombres(); ?>
							</div>
						</td>
						<td>
							<div>
								<strong>Apellidos</strong>
							</div>
							<div>
								<?php echo $model->getApellidos(); ?>
							</div>
						</td>
					</tr>
					<tr>
						<td width="50%">
							<div>
								<strong>Número de registro civil</strong>
							</div>
							<div>
								<?php echo $model->nregistro_civil; ?>
							</div>
						</td>
						<td>
							<div>
								<strong>Certificado Nacido Vivo</strong>
							</div>
							<div>
								<?php echo $model->ncert_nacido_vivo; ?>
							</div>
						</td>
					</tr>
					<tr>
						<td width="50%">
							<div>
								<strong>Dirección</strong>
							</div>
							<div>
								<?php echo $model->direccion; ?>
							</div>
						</td>
						<td>
							<div>
								<strong>Ciudad</strong>
							</div>
							<div>
								<?php echo $model->ciudad; ?>
							</div>
						</td>
					</tr>
					<tr>
						<td width="50%">
							<div>
								<strong>Grupo sanguineo y RH</strong>
							</div>
							<div>
								<?php echo $model->rh; ?>
							</div>
						</td>
						<td>
							<div>
								<strong>Sexo</strong>
							</div>
							<div>
								<?php echo $model->sexo=='M'?'MASCULINO':'FEMENINO'; ?>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<!-- Fin información del paciente -->
	<br />

	<!-- Información del acudiente -->
	<table border="1" cellpadding="5">
		<tr>
			<td style="border:2px #000 solid"><strong>ACUDIENTE</strong></td>
		</tr>
		<tr>
			<td style="padding:15px">
				<?php $acudientes = $model->acudientes; ?>
				<?php foreach($acudientes as $key => $value): ?>
				<h3 style="text-decoration:underline"><?php echo $value->parentesco; ?></h3>
				<table width="100%" cellpadding="5" cellspacing="5">
					<tr>
						<td width="50%">
							<div>
								<strong>Nombres</strong>
							</div>
							<div>
								<?php echo $value->getNombres(); ?>
							</div>
						</td>
						<td>
							<div>
								<strong>Apellidos</strong>
							</div>
							<div>
								<?php echo $value->getApellidos(); ?>
							</div>
						</td>
					</tr>
					<tr>
						<td width="50%">
							<div>
								<strong>Tipo Documento</strong>
							</div>
							<div>
								<?php echo $value->tipoDocumento->titulo; ?>
							</div>
						</td>
						<td>
							<div>
								<strong>Número de Documento</strong>
							</div>
							<div>
								<?php echo $value->documento; ?>
							</div>
						</td>
					</tr>
					<tr>
						<td width="50%">
							<div>
								<strong>Dirección</strong>
							</div>
							<div>
								<?php echo $value->direccion; ?>
							</div>
						</td>
						<td>
							<div>
								<strong>Ciudad</strong>
							</div>
							<div>
								<?php echo $value->ciudad; ?>
							</div>
						</td>
					</tr>
					<tr>
						<td width="50%">
							<div>
								<strong>Teléfono</strong>
							</div>
							<div>
								<?php echo $value->telefono; ?>
							</div>
						</td>
						<td>
							<div>
								<strong>Dirección de correo electrónico</strong>
							</div>
							<div>
								<?php echo $value->correo; ?>
							</div>
						</td>
					</tr>
				</table>
			<?php endforeach ?>
			</td>
		</tr>
	</table>
	<!-- Fin información del acudiente -->
	<br>

	<!-- información de citas y control de desarrollo -->
	<table width="100%">
		<tr>
			<td width="50%" valign="top">
				<table width="100%" border="1" cellpadding="4">
					<tr>
						<th colspan="6" style="background:#F4F4F4">Historico de control de crecimiento</th>
					</tr>
					<tr>
						<th>Fecha</th>
						<th>Edad</th>
						<th>T</th>
						<th>P</th>
						<th>P.C</th>
						<th>Observ.</th>
					</tr>
					<?php $evoluciones = $model->evolucions; ?>
					<?php foreach($evoluciones as $key => $value): ?>
					<tr>
						<td><?php echo $value->fecha; ?></td>
						<td><?php echo $value->edad; ?></td>
						<td><?php echo $value->talla; ?></td>
						<td><?php echo $value->peso; ?></td>
						<td><?php echo $value->perimetro_cefalico; ?></td>
						<td><?php echo $value->observaciones; ?></td>
					</tr>
					<?php endforeach ?>

				</table>
			</td>
			<td valign="top">
				<table width="100%" border="1" cellpadding="4">
					<tr>
						<th colspan="4" style="background:#F4F4F4">Historico de citas</th>
					</tr>
					<tr>
						<th>Fecha y Hora</th>
						<th>Medico/Enfermero</th>
						<th>Motivo</th>
						<th>Estado</th>
					</tr>
					<?php $citas = $model->citaMedicas; ?>
					<?php foreach($citas as $key => $value): ?>
					<tr>
						<td><?php echo $value->fechahora; ?></td>
						<td><?php echo $value->medico->getNombreCompleto(); ?></td>
						<td><?php echo $value->motivo; ?></td>
						<td><?php echo $value->estado == 2?'CANCELADA':(time()>strtotime($value->fechahora)?'CUMPLIDA':'PENDIENTE'); ?></td>
					</tr>
					<?php endforeach ?>

				</table>
			</td>
		</tr>
		<tr>
			<td valign="top">
				<table width="100%" border="1" cellpadding="4">
					<tr>
						<th colspan="3	" style="background:#F4F4F4">Vacunas</th>
					</tr>
					<tr>
						<th>Fecha Aplicación</th>
						<th>Vacuna</th>
						<th>IPS</th>
					</tr>
					<?php $vacuna = $model->vacunaPacientes; ?>
					<?php foreach($vacuna as $key => $value): ?>
					<tr>
						<td><?php echo $value->fecha_aplicacion; ?></td>
						<td><?php echo $value->vacunaedad->vacuna->nombre; ?></td>
						<td><?php echo $value->ips_vacunadora; ?></td>
					</tr>
					<?php endforeach ?>

				</table>
			</td>
			<td valign="top">
				<table width="100%" border="1" cellpadding="4">
					<tr>
						<th colspan="8" style="background:#F4F4F4">Escala de desarrollo</th>
					</tr>
					<?php $escalas = EscalaDesarrollo::model()->findAll(array('order'=>'orden ASC')); ?>
					<tr>
						<th>Fecha</th>
						<th>Edad (meses)</th>
						<?php foreach($escalas as $key => $value): ?>
							<td><?php echo $value->abreviatura; ?></td>
						<?php endforeach ?>
						<th>Total</th>
						<th>Observ.</th>
					</tr>
					<?php $escalares = $model->escalaDesarrolloResultados; ?>
					<?php foreach($escalares as $key => $value): ?>
					<tr>
						<td><?php echo $value->fecha; ?></td>
						<td><?php echo $value->edad; ?></td>
						<?php $total = 0; ?>
						<?php 
							$arr = array();
							foreach($value->escalaDesarrolloResultadoItems as $k => $v){
								$arr[$v->escalaDesarrolloItem->escalaDesarrollo->abreviatura] = $v->escalaDesarrolloItem->valor;
								$total += $v->escalaDesarrolloItem->valor;;
							} 
						?>
						<?php foreach($escalas as $k => $v): ?>
							<td><?php echo $arr[$v->abreviatura]; ?></td>
						<?php endforeach ?>
						<td><?php echo $total; ?></td>
						<td><?php echo $value->observaciones; ?></td>
					</tr>
					<?php endforeach ?>
				</table>
			</td>
		</tr>
	</table>
	<!-- Fin de información de citas y control de desarrollo -->

	<br><br>
	<center>Montería <?php echo date('Y'); ?></center>

</body>
</html>