<?php 
$grouped = array();
foreach ($vacunas as $key => $value) {
	$index = "EDAD_ID{$value->edad->id}";
	$grouped[$index][$value->id]['vacuna'] = $value;
}

foreach ($model->vacunaPacientes as $key => $value) {
	$index = "EDAD_ID{$value->vacunaedad->edad->id}";
	if(isset($grouped[$index][$value->vacuna])){
		$grouped[$index][$value->vacuna]['aplicada'] = $value;
	}
}
 ?>

<div class="page-title">
	<div class="title_left">
		<h3>Vacunas de <?php echo $model->getNombreCompleto(); ?></h3>
	</div>
	
		<a href="<?php echo $this->createUrl('paciente/view',array('id'=>$model->id)); ?>" class="btn btn-success pull-right"><i class="glyphicon glyphicon-user"></i> Información del paciente</a>
	
</div>
<div class="clearfix"></div>

<?php if(Yii::app()->user->hasFlash('error')): ?>
<div class="alert alert-danger alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php endif ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
<div class="alert alert-success alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php endif ?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">	
		<div class="x_panel">			
			<div class="x_content">
			<table class="table table-bordered mytable">
				<tr class="active">
					<th>Edad</th>
					<th width="25%">Me protege de</th>
					<th>Dosis</th>
					<th width="85px">Fecha de Aplicación</th>
					<th>Laboratorio</th>
					<th>Número de lote</th>
					<th>IPS Vacunadora</th>
					<th width="85px">Fecha proxima cita</th>
					<th>Nombre del vacunador</th>
				</tr>
				<?php $showapply = true; ?>
				<?php foreach ($grouped as $key => $value): ?>
				<?php 					
						$count = count($value); 
						$first = true;
					?>

					<?php foreach ($value as $key2 => $value2):?>
						<tr>
						<?php if($first): ?>
							<th style="vertical-align: middle" rowspan="<?php echo $count; ?>"><?php echo $value2['vacuna']->edad->descripcion; ?></th>
							<?php $first = false; ?>
						<?php endif ?>
							<td style="vertical-align: middle;"><?php echo $value2['vacuna']->vacuna->nombre; ?></td>
							<td style="text-align: center; vertical-align: middle;"><?php echo $value2['vacuna']->dosis; ?></td>
							<?php if(isset($value2['aplicada'])): ?>
								<?php $apply = $value2['aplicada']; ?>
								<td style="vertical-align: middle;"><?php echo $apply->fecha_aplicacion; ?></td>
								<td style="vertical-align: middle;"><?php echo $apply->laboratorio; ?></td>
								<td style="vertical-align: middle;"><?php echo $apply->numero_lote; ?></td>
								<td style="vertical-align: middle;"><?php echo $apply->ips_vacunadora; ?></td>
								<td style="vertical-align: middle;"><?php echo $apply->fecha_proxima; ?></td>
								<td style="vertical-align: middle;"><?php echo $apply->vacunador; ?></td>
							<?php else: ?>
								<td colspan="6" style="vertical-align: middle">
									<?php if($showapply): ?>
										<?php $poraplicar = $value2['vacuna']; ?>
										<?php $showapply = false; ?>
									<a href="#" class="btn btn-xs btn-primary" data-toggle="modal" data-target=".aplicarvacuna"> <i class="glyphicon glyphicon-ok"></i> Aplicar Vacuna</a>
									<?php endif ?>
								</td>
							<?php endif ?>
						</tr>
						
					<?php endforeach; ?>
				<?php endforeach; ?>
			</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade aplicarvacuna" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">Aplicar vacuna <?php echo $poraplicar->vacuna->nombre; ?></h4>
      </div>
      <div class="modal-body">
      	<?php $modelform = new VacunaPaciente; ?>
        <?php $form = $this->beginWidget('CActiveForm',array(
        		'action'=>array('aplicarVacuna'),
	        	'id'=>'form-aplicarvacuna',
	        	'enableAjaxValidation'=>false,
	        	'enableClientValidation'=>true,
	        	'clientOptions'=>array(
	        		'validateOnSubmit'=>true
	        	)
        	  ));        	
       	?>
       		<?php echo $form->hiddenField($modelform,'paciente_id',array('value'=>$model->id)); ?>
       		<?php echo $form->hiddenField($modelform,'vacuna',array('value'=>$poraplicar->id)); ?>
       		<div class="row">
       			<div class="col-xs-12 col-sm-6 col-md-6">
		       		<div class="form-group">
		       			<?php echo $form->labelEx($modelform,'fecha_aplicacion'); ?>
		       			<?php echo $form->dateField($modelform,'fecha_aplicacion',array('class'=>'form-control','value'=>date('Y-m-d'))); ?>
		       			<?php echo $form->error($modelform,'fecha_aplicacion',array('class'=>'text-danger')); ?>
		       		</div>
       			</div>
       			<div class="col-xs-12 col-sm-6 col-md-6">
		       		<div class="form-group">
		       			<?php echo $form->labelEx($modelform,'fecha_proxima'); ?>
		       			<?php echo $form->dateField($modelform,'fecha_proxima',array('class'=>'form-control')); ?>
		       			<?php echo $form->error($modelform,'fecha_proxima',array('class'=>'text-danger')); ?>
		       		</div>
       			</div>
       		</div>
       		
       		<div class="form-group">
       			<?php echo $form->labelEx($modelform,'laboratorio'); ?>
       			<?php echo $form->textField($modelform,'laboratorio',array('class'=>'form-control')); ?>
       			<?php echo $form->error($modelform,'laboratorio',array('class'=>'text-danger')); ?>
       		</div>
       		<div class="form-group">
       			<?php echo $form->labelEx($modelform,'numero_lote'); ?>
       			<?php echo $form->textField($modelform,'numero_lote',array('class'=>'form-control')); ?>
       			<?php echo $form->error($modelform,'numero_lote',array('class'=>'text-danger')); ?>
       		</div>
       		<div class="form-group">
       			<?php echo $form->labelEx($modelform,'ips_vacunadora'); ?>
       			<?php echo $form->textField($modelform,'ips_vacunadora',array('class'=>'form-control')); ?>
       			<?php echo $form->error($modelform,'ips_vacunadora',array('class'=>'text-danger')); ?>
       		</div>
       		<div class="form-group">
       			<?php echo $form->labelEx($modelform,'vacunador'); ?>
       			<?php echo $form->textField($modelform,'vacunador',array('class'=>'form-control')); ?>
       			<?php echo $form->error($modelform,'vacunador',array('class'=>'text-danger')); ?>
       		</div>       		

       		<div>
       		<?php 
       			echo CHtml::ajaxSubmitButton('Aplicar Vacuna', CHtml::normalizeUrl(array('/paciente/aplicarVacuna','render'=>true)),
       			array(
       				'dataType'=>'json',
       				'type'=>'POST',
       				'success'=>'function(data){
       					if(data.success){
							location.reload();
       					}
       					else{
       						alert(data.message);
       					}
       				}'
       			),
       			array('class'=>'btn btn-success','confirm'=>'Confirme que ha verificado la información ingresada y desea continuar.')); 
       		?></div>
        <?php $this->endWidget(); ?>
      </div>
    </div>
  </div>
</div>