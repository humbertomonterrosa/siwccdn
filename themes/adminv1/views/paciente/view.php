<div class="page-title">
	<div class="title_left">
		<h3>Historia de paciente</h3>
	</div>
	<div class="title_right">
		<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
		    <div class="input-group">
                <?php echo CHtml::beginForm(array('/paciente'),'POST',array('style'=>'display:inherit')); ?>
		        <input type="text" class="form-control" name="BuscaPaciente[nombre]" placeholder="Nombre del paciente">
		        <span class="input-group-btn">
		              <button class="btn btn-default" type="button">Búscar</button>
		        </span>
                <?php echo CHtml::endForm(); ?>
		    </div>            
		</div>
        <a href="<?php echo $this->createUrl('vacunas',array('id'=>$model->id)); ?>" class="btn btn-success pull-right" style="margin-right:10px"><i class="glyphicon glyphicon-book"></i> Vacunas</a> <a href="<?php echo $this->createUrl('escalaDesarrollo',array('id'=>$model->id)); ?>" class="btn btn-success pull-right" style="margin-right:10px"><i class="glyphicon glyphicon-signal"></i> Escala de desarrollo</a>
	</div>
</div>
<div class="clearfix"></div>
<?php if(Yii::app()->user->hasFlash('error')): ?>
<div class="alert alert-danger alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php endif ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
<div class="alert alert-success alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php endif ?>
<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_panel" style="height:460px">
			<div class="x_title">
				<h2>
					<li class="fa fa-info"></li> Información del paciente
                </h2>
                <a href="<?php echo $this->createUrl('reporte',array('id'=>$model->id)); ?>" target="_blank" class="btn btn-success btn-xs pull-right"><li class="glyphicon glyphicon-eye-open"></li> Ver reporte general</a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<?php $this->widget('ext.yiibooster.widgets.TbDetailView',array(
					'data'=>$model,
					'type'=>'striped bordered condensed',
					'attributes'=>array(
						array('name'=>'Nombre completo','value'=>$model->getNombreCompleto()),
						array('name'=>'Fecha Nacimiento','value'=>date("d/m/Y",strtotime($model->fecha_nacimiento))),
						'nregistro_civil',
						'ncert_nacido_vivo',
						array('name'=>'ciudad','value'=>CHtml::encode($model->ciudad)),
						array('name'=>'Sexo','value'=>$model->sexo=='F'?'FEMENINO':'MASCULINO'),
						)
				)); ?>
                <br />
                <div class="x_title">Acudiente</div>
                <?php $acudiente = $model->acudientes[0]; ?>
                <?php $this->widget('ext.yiibooster.widgets.TbDetailView',array(
                    'data'=>$acudiente,
                    'type'=>'striped bordered condensed',
                    'attributes'=>array(
                        array('name'=>'Nombre completo','value'=>$acudiente->getNombreCompleto()),
                        array('name'=>'Paresteco','value'=>CHtml::encode($acudiente->parentesco)),
                        array('name'=>'Telefono','value'=>CHtml::encode($acudiente->telefono)),
                        )
                )); ?>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_panel" style="height:460px">
			<div class="x_title">
				<h2>
					<li class="fa fa-calendar"></li> Calendario de citas
				</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
                <div id="calendar"></div>				
			</div>
		</div>
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>
					<li class="fa fa-line-chart"></li> Crecimiento y desarrollo
				</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<?php if(Yii::app()->user->hasFlash('error_evolucion')): ?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <?php echo Yii::app()->user->getFlash('error_evolucion'); ?>
                    <script>$(document).on('ready',function(){$("#registro").modal()});</script>
                </div>
				<?php endif ?>
				<?php if(Yii::app()->user->hasFlash('success_evolucion')): ?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <?php echo Yii::app()->user->getFlash('success_evolucion'); ?>
                </div>
				<?php endif ?>
				<div class="row">
					<div class="col-md-3 col-sm-3 col-xs-3">
						<div class="x_title"><strong>Controles</strong></div>
						<ul class="list-group">
							<?php $proximo = $model->getProximoControl(); ?>
							<?php foreach($model->getControles() as $key => $value): ?>
							<li class="list-group-item">
								<span class="fa fa-<?php echo $value['status']=='OK'?'check text-success':($proximo->id==$value['id']?'arrow-right':'clock-o');?>"></span>
								<a href="#control<?php echo $value['id'];?>"><?php echo $value['titulo']; ?></a>
								<?php if($proximo->id==$value['id']): ?>
									<a href="#" data-target="#registro" data-toggle="modal" class="btn btn-primary btn-xs btn-round pull-right">Registrar</a>
								<?php else: ?>
									<span class="pull-right">
										<?php echo '<span class="text-'.($value['status']=='OK'?'success">Registrado':'disabled">Pendiente').'</span>'; ?>
									</span>
								<?php endif; ?>
							</li>
							<?php endforeach; ?>
						</ul>
						<!-- modal -->
						<div id="registro" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel2">Registro de control de crecimiento y desarrollo</h4>
                                    </div>
                                    
                                    <?php echo $this->renderPartial('insert_evolucion',array('evolucion'=>$evolucion,'model'=>$model)); ?>
                                    

                                </div>
                            </div>
                        </div><!-- end modal -->
						<a class="btn btn-sm btn-success btn-round btn-block" href="<?php echo $this->createUrl('/citas/agendar/'.$model->id); ?>"><span class="fa fa-calendar"></span> Solicitar próxima cita de control</a>
					</div>

					<div class="col-md-9 col-sm-9 col-xs-9">
						    <?php $sexo = $model->sexo=='F'?'Niñas':'Niños'; ?>
				        	<h3>Talla para la edad <?php echo $sexo; ?> de <?php echo $model->getEvaluacionActual()->titulo; ?></h3>
				            <canvas id="canvas-talla"></canvas>
				        	<h3>Peso para la edad <?php echo $sexo; ?> de <?php echo $model->getEvaluacionActual()->titulo; ?></h3>
				            <canvas id="canvas-peso"></canvas>
				        	<h3>Perimetro cefalico para la edad <?php echo $sexo; ?> de <?php echo $model->getEvaluacionActual()->titulo; ?></h3>
				            <canvas id="canvas-pc"></canvas>
                            <h3>Peso para la talla <?php echo $sexo; ?> de <?php echo $model->getEvaluacionActual()->titulo; ?></h3>
                            <canvas id="canvas-pt"></canvas>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $sexo = $model->sexo=='F'?'ninas':'ninos';  ?>
<?php $cs = Yii::app()->clientScript; ?>
<?php $labels = array(); ?>
<?php $tallas = array(); ?>
<?php $pesos = array(); ?>
<?php $pcs = array(); ?>
<?php $pts = array(); ?>

<?php $reftalla = MedidasOMS::get('talla_'.$sexo); ?>
<?php $refpeso = MedidasOMS::get('peso_'.$sexo); ?>
<?php $refpc = MedidasOMS::get('pc_'.$sexo); ?>
<?php $refpt = MedidasOMS::get('peso_talla_'.$sexo); ?>

<?php 
    $arrtmp = array();
    foreach ($refpt as $key => $value) {
        $index = array_shift($value);
        $arrtmp[$index] = $value;
    }
    $refpt = $arrtmp;
 ?>

<?php $maximomes = $model->getEvaluacionActual()->fin/30; ?>
<?php $mes = 0; ?>
<?php foreach ($model->evolucions as $key => $value) {
    	if(!$key) $mes = 0;
    	else {
    		$mes = $value->edad;
    	}

    	$tallas[] = number_format($value->talla,2,".","");
    	$pesos[] = number_format($value->peso,2,".","");
    	$pcs[] = number_format($value->perimetro_cefalico,2,".","");
    	$labels[] = $mes.'m';

    }

    foreach ($tallas as $key => $value) {
        $talla_tmp = number_format($value,1,'.','');
        $pts[] = @$refpt[$talla_tmp][4];
    }
 ?>
<?php if($mes<$maximomes){
	for ($i=$mes+1; $i <= $maximomes; $i++) { 
		$labels[] = $i."m";
	}
} ?>
<?php $ref = array(); ?>
<?php foreach ($labels as $key => $value) {
		$ref['t_sdn2'][] = @number_format($reftalla[$value*30][3],2,'.','');
		$ref['t_sdn1'][] = @number_format($reftalla[$value*30][4],2,'.','');
		$ref['t_sd0'][] = @number_format($reftalla[$value*30][5],2,'.','');
		$ref['t_sd1'][] = @number_format($reftalla[$value*30][6],2,'.','');
		$ref['t_sd2'][] = @number_format($reftalla[$value*30][7],2,'.','');

		$ref['p_sdn2'][] = @number_format($refpeso[$value*30][3],2,'.','');
		$ref['p_sdn1'][] = @number_format($refpeso[$value*30][4],2,'.','');
		$ref['p_sd0'][] = @number_format($refpeso[$value*30][5],2,'.','');
		$ref['p_sd1'][] = @number_format($refpeso[$value*30][6],2,'.','');
		$ref['p_sd2'][] = @number_format($refpeso[$value*30][7],2,'.','');
		$ref['p_sd3'][] = @number_format($refpeso[$value*30][8],2,'.','');

		$ref['pc_sdn2'][] = @number_format($refpc[$value*30][3],2,'.','');
		$ref['pc_sdn1'][] = @number_format($refpc[$value*30][4],2,'.','');
		$ref['pc_sd0'][] = @number_format($refpc[$value*30][5],2,'.','');
		$ref['pc_sd1'][] = @number_format($refpc[$value*30][6],2,'.','');
		$ref['pc_sd2'][] = @number_format($refpc[$value*30][7],2,'.','');


} 
    $labelspt = array();
    $tallas_tmp = 0;
    foreach ($tallas as $key => $value) {
        $tallas_tmp = number_format($value,1,'.','');
        $labelspt[] = "$tallas_tmp cm";
        $ref['pt_sdn3'][] = @number_format($refpt[$tallas_tmp][1],1,'.','');
        $ref['pt_sdn2'][] = @number_format($refpt[$tallas_tmp][2],1,'.','');
        $ref['pt_sdn1'][] = @number_format($refpt[$tallas_tmp][3],1,'.','');
        $ref['pt_sd0'][] = @number_format($refpt[$tallas_tmp][4],1,'.','');
        $ref['pt_sd1'][] = @number_format($refpt[$tallas_tmp][5],1,'.','');
        $ref['pt_sd2'][] = @number_format($refpt[$tallas_tmp][6],1,'.','');
        $ref['pt_sd3'][] = @number_format($refpt[$tallas_tmp][7],1,'.','');
        
    }

    if($tallas_tmp===0) $tallas_tmp = 45;

    for($i=$tallas_tmp+5;$i<=110.0;$i+=5){
        $labelspt[] = "$i cm";
        $index = number_format($i,1,'.','');
        $ref['pt_sdn3'][] = @number_format($refpt[$index][1],1,'.','');
        $ref['pt_sdn2'][] = @number_format($refpt[$index][2],1,'.','');
        $ref['pt_sdn1'][] = @number_format($refpt[$index][3],1,'.','');
        $ref['pt_sd0'][] = @number_format($refpt[$index][4],1,'.','');
        $ref['pt_sd1'][] = @number_format($refpt[$index][5],1,'.','');
        $ref['pt_sd2'][] = @number_format($refpt[$index][6],1,'.','');
        $ref['pt_sd3'][] = @number_format($refpt[$index][7],1,'.','');
    }
?>


<?php $cs->registerScript('linechart','
	 var lineChartData = {
            labels: '.json_encode($labels).',

            datasets: [
            {
                    label: "sdn2",
                    strokeColor: "#cc0000",
                    pointColor: "#cc0000",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['t_sdn2']).',
            },
            {
                    label: "sdn1",
                    strokeColor: "#999999",
                    pointColor: "#999999",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['t_sdn1']).'
            },
            {
                    label: "sd0",
                    strokeColor: "#000000",
                    pointColor: "#000000",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['t_sd0']).'
            },
            {
                    label: "sd1",
                    strokeColor: "#999999",
                    pointColor: "#999999",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['t_sd1']).'
            },
            {
                    label: "sd2",
                    strokeColor: "#cc0000",
                    pointColor: "#cc0000",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['t_sd2']).'
            },
            {
                    label: "Pareciente",
                    strokeColor: "#00ff00",
                    pointColor: "#00ff00",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($tallas).'
            },
        ]

        }

        $(document).ready(function () {
            new Chart(document.getElementById("canvas-talla").getContext("2d")).Line(lineChartData, {
                responsive: true,
                tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                datasetFill: false,
            });
        });

	var lineChartData2 = {
            labels: '.json_encode($labels).',
            datasets: [
            {
                    label: "sdn2",
                    strokeColor: "#cc0000",
                    pointColor: "#cc0000",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['p_sdn2']).'
            },
            {
                    label: "sdn1",
                    strokeColor: "#999999",
                    pointColor: "#999999",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['p_sdn1']).'
            },
            {
                    label: "sd0",
                    strokeColor: "#000000",
                    pointColor: "#000000",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['p_sd0']).'
            },
            {
                    label: "sd1",
                    strokeColor: "#999999",
                    pointColor: "#999999",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['p_sd1']).'
            },
            {
                    label: "sd2",
                    strokeColor: "#cccccc",
                    pointColor: "#cccccc",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['p_sd2']).'
            },
            {
                    label: "sd3",
                    strokeColor: "#cc0000",
                    pointColor: "#cc0000",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['p_sd3']).'
            },
            {
                    label: "Pareciente",
                    strokeColor: "#00ff00",
                    pointColor: "#00ff00",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($pesos).'
            },
        ]

        }

        $(document).ready(function () {
            new Chart(document.getElementById("canvas-peso").getContext("2d")).Line(lineChartData2, {
                responsive: true,
                tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                datasetFill: false,
            });
        });

	var lineChartData3 = {
            labels: '.json_encode($labels).',
            datasets: [
            {
                    label: "sdn2",
                    strokeColor: "#cc0000",
                    pointColor: "#cc0000",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['pc_sdn2']).'
            },
            {
                    label: "sdn1",
                    strokeColor: "#999999",
                    pointColor: "#999999",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['pc_sdn1']).'
            },
            {
                    label: "sd0",
                    strokeColor: "#000000",
                    pointColor: "#000000",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['pc_sd0']).'
            },
            {
                    label: "sd1",
                    strokeColor: "#999999",
                    pointColor: "#999999",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['pc_sd1']).'
            },
            {
                    label: "sd2",
                    strokeColor: "#cc0000",
                    pointColor: "#cc0000",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['pc_sd2']).'
            },
            {
                    label: "Pareciente",
                    strokeColor: "#00ff00",
                    pointColor: "#00ff00",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($pcs).'
            },
        ]

        }

        $(document).ready(function () {
            new Chart(document.getElementById("canvas-pc").getContext("2d")).Line(lineChartData3, {
                responsive: true,
                tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                datasetFill: false,
            });
        });

        var lineChartData4 = {
            labels: '.json_encode($labelspt).',
            datasets: [
            {
                    label: "sdn3",
                    strokeColor: "#cc0000",
                    pointColor: "#cc0000",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['pt_sdn3']).'
            },
            {
                    label: "sdn2",
                    strokeColor: "#cc0000",
                    pointColor: "#cc0000",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['pt_sdn2']).'
            },
            {
                    label: "sdn1",
                    strokeColor: "#999999",
                    pointColor: "#999999",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['pt_sdn1']).'
            },
            {
                    label: "sd0",
                    strokeColor: "#000000",
                    pointColor: "#000000",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['pt_sd0']).'
            },
            {
                    label: "sd1",
                    strokeColor: "#999999",
                    pointColor: "#999999",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['pt_sd1']).'
            },
            {
                    label: "sd2",
                    strokeColor: "#cc0000",
                    pointColor: "#cc0000",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['pt_sd2']).'
            },
            {
                    label: "sd3",
                    strokeColor: "#cc0000",
                    pointColor: "#cc0000",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($ref['pt_sd3']).'
            },
            {
                    label: "Pareciente",
                    strokeColor: "#00ff00",
                    pointColor: "#00ff00",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: '.json_encode($pts).'
            },
            
        ]

        }

        $(document).ready(function () {
            new Chart(document.getElementById("canvas-pt").getContext("2d")).Line(lineChartData4, {
                responsive: true,
                tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                datasetFill: false,
            });
        });


'); 

$citas = $model->citaPendientes;
$defaultDate = $citas?date('Y-m-d',strtotime($citas[0]->fechahora)):date('Y-m-d');
$events = array();
foreach ($citas as $key => $value) {
    $events[] = array("title"=>$value->motivo,"start"=>$value->fechahora);
    
}

$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/calendar/fullcalendar.css');
$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/calendar/fullcalendar.min.js',CClientScript::POS_END);
$cs->registerScript('calendar','
    $("#calendar").fullCalendar({
        header: {
            left: "prev,next today",
            center: "title",
            right: "month,agendaWeek,agendaDay"
        },
        defaultView:"agendaDay",
        defaultDate: "'.$defaultDate.'",
        eventLimit: true,
        events: '.json_encode($events).',        
    })',CClientScript::POS_END);

?>
