<?php $this->layout = "/layouts/login"; ?>
<div class="">
        <div id="wrapper">
            <div id="login" class="animate form">
                <section class="login_content">
					<?php $form = $this->beginWidget('CActiveForm',array(
						'id'=>'login-form',
						'enableClientValidation'=>false
					)); ?>
                        <h1>Inicio de sesión</h1>
                        <div class="form-group">
                        	<?php echo $form->textField($model,'username',array('placeholder'=>'Nombre de usuario','class'=>'form-control','required'=>'required')); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $form->passwordField($model,'password',array('placeholder'=>'Contraseña','class'=>'form-control','required'=>'required')); ?>
                            <?php echo $form->error($model,'password',array('class'=>'text-danger','style'=>'margin-top: -10px')); ?>
                        </div>
                        <div>
                            <?php echo CHtml::submitButton('Iniciar Sesión',array('class'=>'btn btn-default submit btn-flat')); ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">
                            <div>
                                <h1><?php echo Yii::app()->name; ?></h1>

                                <p>©2015 All Rights Reserved.</p>
                            </div>
                        </div>
                       <?php $this->endWidget(); ?>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
        </div>
    </div>